line2 = [
    "NAME:PolylineParameters",
    "IsPolylineCovered:=", True,
    "IsPolylineClosed:=", True,
    [
        "NAME:PolylinePoints",
        [
            "NAME:PLPoint",
            "X:=", "0mm",
            "Y:=", "0mm",
            "Z:=", "0mm"
        ],
        [
            "NAME:PLPoint",
            "X:=", "0mm",
            "Y:=", "5mm",
            "Z:=", "0mm"
        ],
        [
            "NAME:PLPoint",
            "X:=", "5mm",
            "Y:=", "5mm",
            "Z:=", "0mm"
        ],
    ],
    [
        "NAME:PolylineSegments",
        [
            "NAME:PLSegment",
            "SegmentType:=", "Line",
            "StartIndex:=", 0,
            "NoOfPoints:=", 2
        ],
        [
            "NAME:PLSegment",
            "SegmentType:=", "Line",
            "StartIndex:=", 1,
            "NoOfPoints:=", 2
        ],
        [
            "NAME:PLSegment",
            "SegmentType:=", "Line",
            "StartIndex:=", 2,
            "NoOfPoints:=", 2
        ]
    ],
    [
        "NAME:PolylineXSection",
        "XSectionType:=", "None",
        "XSectionOrient:=", "Auto",
        "XSectionWidth:=", "0mm",
        "XSectionTopWidth:=", "0mm",
        "XSectionHeight:=", "0mm",
        "XSectionNumSegments:=", "0",
        "XSectionBendType:=", "Corner"
    ]
]
