import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import scipy.stats
import matplotlib.animation as animation


def minimaFunction(params):
    # Bivariate Normal function
    X, Y = params
    sigma11, sigma12, mu11, mu12 = (3.0, .5, 0.0, 0.0)
    Z1 = mlab.bivariate_normal(X, Y, sigma11, sigma12, mu11, mu12)
    Z = Z1
    return -40 * Z


def minimaFunctionDerivative(params):
    # Derivative of the bivariate normal function
    X, Y = params
    sigma11, sigma12, mu11, mu12 = (3.0, .5, 0.0, 0.0)
    dZ1X = -scipy.stats.norm.pdf(X, mu11, sigma11) * (mu11 - X) / sigma11 ** 2
    dZ1Y = -scipy.stats.norm.pdf(Y, mu12, sigma12) * (mu12 - Y) / sigma12 ** 2
    return (dZ1X, dZ1Y)


def optimize(iterations, oF, dOF, params, learningRate, beta1, beta2):
    """
    computes the optimal value of params for a given objective function and its derivative
    Arguments:
        - iteratoins - the number of iterations required to optimize the objective function
        - oF - the objective function
        - dOF - the derivative function of the objective function
        - params - the parameters of the function to optimize
        - learningRate - the learning rate
- beta1 - The weighted moving average parameter for momentum component of ADAM
- beta2 - The weighted moving average parameter for RMSProp component of ADAM
    Return:
        - oParams - the list of optimized parameters at each step of iteration
    """
    oParams = [params]
    vdw = (0.0, 0.0)
    sdw = (0.0, 0.0)
    vdwCorr = (0.0, 0.0)
    sdwCorr = (0.0, 0.0)
    eps = 10 ** (-7)
    # The iteration loop
    for i in range(iterations):
        # Compute the derivative of the parameters
        dParams = dOF(params)
        # Compute the momentum of each gradient vdw = vdw*beta+(1.0+beta)*dPar
        vdw = tuple([vDW * beta1 + (1.0 - beta1) * dPar for dPar, vDW in zip(dParams, vdw)])
        # Compute the rms of each gradient sdw = sdw*beta+(1.0+beta)*dPar^2
        sdw = tuple([sDW * beta2 + (1.0 - beta2) * dPar ** 2.0 for dPar, sDW in zip(dParams, sdw)])
        # Compute the weight boosting for sdw and vdw
        vdwCorr = tuple([vDW / (1.0 - beta1 ** (i + 1.0)) for vDW in vdw])
        sdwCorr = tuple([sDW / (1.0 - beta2 ** (i + 1.0)) for sDW in sdw])
        # SGD in this line Goes through each parameter and applies parameter = parameter -learningrate*dParameter
        params = tuple([par - learningRate * vdwCORR / ((sdwCORR ** .5) + eps) for sdwCORR, vdwCORR, par in
                        zip(vdwCorr, sdwCorr, params)])
        # append the new parameters
        oParams.append(params)
    return oParams


iterations = 100
learningRate = .1
beta1 = .9
beta2 = .999
x, y = 5.0, 1.0
params = (x, y)
optimizedParameters = optimize(iterations, \
                               minimaFunction, \
                               minimaFunctionDerivative, \
                               params, \
                               learningRate, \
                               beta1, \
                               beta2)
