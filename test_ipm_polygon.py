from unittest import TestCase

from polygon import ipm_polygon
import math


class TestIpm_polygon(TestCase):

    def test_gen_PM(self):
        ipm_polygon(sides=10, radius_range_low=0.5, radius_range_high=1,
                    theta_range_low=0 / 180 * math.pi,
                    theta_range_high=90 / 180 * math.pi, pm_size=100)


if __name__ == '__main__':
    TestIpm_polygon.main()
