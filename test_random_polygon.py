from unittest import TestCase
from polygon import random_poly
from shapely.geometry import Point, Polygon
import math
from matplotlib import pyplot as plt


class TestRandom_polygon(TestCase):
    def test_random_polygon(self):
        poly = Polygon([(0, 0), (0, 1), (2, 2), (2, 0)])
        x, y = poly.exterior.xy
        plt.plot(x, y)

        flux_barrier1_list = random_poly(Point(0, 1), Point(0, 0), sides=5,
                                         radius_range_low=0, radius_range_high=5,
                                         theta_range_low=0,
                                         theta_range_high=math.pi / 2, poly=poly)
        flux_barrier1 = Polygon([p.x, p.y] for p in flux_barrier1_list)
        flux_barrier1_x, flux_barrier1_y = flux_barrier1.exterior.xy
        plt.fill(flux_barrier1_x, flux_barrier1_y, 'r')
        plist = list(zip(*flux_barrier1.exterior.coords.xy))[0:-2]
        counter = 0;
        for p in plist:
            plt.text(p[0], p[1], 'Point%d' % counter)
            counter += 1
        plt.show()
