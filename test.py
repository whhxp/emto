from point_class import Point

outfile = "7"
p_list = [Point(59.62012251932086, 19.482640557769745), Point(9.269612670760116, 3.8603377670515227),
          Point(4.285207202099532, 4.071176666318667), Point(4.157546354138882, 6.613220961518101),
          Point(5.873314096239297, 1.9947574649268929), Point(0.28408393252430164, 5.705723307565613),
          Point(2.105699730092292, 2.3991672912934447), Point(9.683940836875497, 0.4338202392161694),
          Point(9.249570982595971, 6.958461448596152), Point(2.900535178074788, 0.8811843834852295)]
import clr

clr.AddReference("System.Windows.Forms")
clr.AddReference("System.Drawing")
clr.AddReference('System.Xml.Linq')
import sys

sys.path.append('E:\Program Files\AnsysEM\AnsysEM18.1\Win64\PythonFiles\DesktopPlugin')
sys.path.append('E:\Program Files\AnsysEM\AnsysEM18.1\Win64')
import math
import os

from System.Xml.Linq import *

try:
    import System.Drawing
    import System.Windows.Forms
    import ScriptEnv

    ScriptEnv.Initialize("Ansoft.ElectronicsDesktop")
    # oDesktop.RestoreWindow()#
    oProject = oDesktop.NewProject()
    pathlist = os.path.split((os.path.abspath('ipm_' + outfile + '.py')))
    save_name = pathlist[1].split('.')
    save_file = pathlist[0] + '\\vars\\' + save_name[0] + ".aedt"
    # System.Windows.Forms.MessageBox.Show(save_file, "Warning")
    oProject.SaveAs(save_file, True)

    # oProject = oDesktop.SetActiveProject("Project2")
    oProject.InsertDesign("Maxwell 2D", "Maxwell2DDesign1", "Magnetostatic", "")
    oDesign = oProject.SetActiveDesign("Maxwell2DDesign1")
    oDesign.RenameDesignInstance("Maxwell2DDesign1", "ipm1")
    oDesign.SetSolutionType("Transient", "XY")
    oEditor = oDesign.SetActiveEditor("3D Modeler")
    unit = "mm"
    # for arg in sys.argv:
    # 	System.Windows.Forms.MessageBox.Show(arg, "Warning")

    '''
        @working_mode: 'whole' or 'partial'
        if working mode is 'whole', it means the motor will be solved in whole
        if working mode is 'partial', the motor will be solved in partial, i.e. one eighth
    '''
    working_mode = "partial"
except:
    print('-----init Error and application quit')
    oDesktop.QuitApplication()
    exit(-1)


def load_points(filename):
    poly = XDocument().Load(filename)
    print
    '-----load_point finished'
    return poly


def draw_stator():
    global oProject, oDesign
    oEditor = oDesign.SetActiveEditor("3D Modeler")
    oEditor.CreateUserDefinedPart(
        [
            "NAME:UserDefinedPrimitiveParameters",
            "DllName:=", "RMxprt/SlotCore.dll",
            "Version:=", "12.1",
            "NoOfParameters:=", 19,
            "Library:=", "syslib",
            [
                "NAME:ParamVector",
                [
                    "NAME:Pair",
                    "Name:=", "DiaGap",
                    "Value:=", "161.90000000000001mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "DiaYoke",
                    "Value:=", "269.24000000000001mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Length",
                    "Value:=", "0mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Skew",
                    "Value:=", "0deg"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Slots",
                    "Value:=", "48"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "SlotType",
                    "Value:=", "2"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Hs0",
                    "Value:=", "1.03mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Hs01",
                    "Value:=", "0mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Hs1",
                    "Value:=", "0mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Hs2",
                    "Value:=", "29.5mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Bs0",
                    "Value:=", "1.93mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Bs1",
                    "Value:=", "5mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Bs2",
                    "Value:=", "8mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Rs",
                    "Value:=", "5mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "FilletType",
                    "Value:=", "0"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "HalfSlot",
                    "Value:=", "0"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "SegAngle",
                    "Value:=", "0deg"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "LenRegion",
                    "Value:=", "200mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "InfoCore",
                    "Value:=", "0"
                ]
            ]
        ],
        [
            "NAME:Attributes",
            "Name:=", "SlotCore1",
            "Flags:=", "",
            "Color:=", "(128 128 255)",
            "Transparency:=", 0,
            "PartCoordinateSystem:=", "Global",
            "UDMId:=", "",
            "MaterialValue:=", "\"M19_29G\"",
            "SurfaceMaterialValue:=", "\"\"",
            "SolveInside:=", True,
            "IsMaterialEditable:=", True
        ])
    oEditor.CreateUserDefinedPart(
        [
            "NAME:UserDefinedPrimitiveParameters",
            "DllName:=", "RMxprt/LapCoil.dll",
            "Version:=", "16.0",
            "NoOfParameters:=", 22,
            "Library:=", "syslib",
            [
                "NAME:ParamVector",
                [
                    "NAME:Pair",
                    "Name:=", "DiaGap",
                    "Value:=", "161.90000000000001mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "DiaYoke",
                    "Value:=", "269.24000000000001mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Length",
                    "Value:=", "0mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Skew",
                    "Value:=", "0deg"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Slots",
                    "Value:=", "48"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "SlotType",
                    "Value:=", "0"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Hs0",
                    "Value:=", "1.28mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Hs1",
                    "Value:=", "1mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Hs2",
                    "Value:=", "10mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Bs0",
                    "Value:=", "2.5mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Bs1",
                    "Value:=", "8mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Bs2",
                    "Value:=", "5mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Rs",
                    "Value:=", "0mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "FilletType",
                    "Value:=", "0"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "Layers",
                    "Value:=", "2"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "CoilPitch",
                    "Value:=", "4"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "EndExt",
                    "Value:=", "5mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "SpanExt",
                    "Value:=", "25mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "BendAngle",
                    "Value:=", "0deg"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "SegAngle",
                    "Value:=", "10deg"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "LenRegion",
                    "Value:=", "200mm"
                ],
                [
                    "NAME:Pair",
                    "Name:=", "InfoCoil",
                    "Value:=", "0"
                ]
            ]
        ],
        [
            "NAME:Attributes",
            "Name:=", "LapCoil1",
            "Flags:=", "",
            "Color:=", "(143 175 143)",
            "Transparency:=", 0,
            "PartCoordinateSystem:=", "Global",
            "UDMId:=", "",
            "MaterialValue:=", "\"copper\"",
            "SurfaceMaterialValue:=", "\"\"",
            "SolveInside:=", True,
            "IsMaterialEditable:=", True
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DCmdTab",
                [
                    "NAME:PropServers",
                    "LapCoil1:CreateUserDefinedPart:1"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Hs1",
                        "Value:=", "0.1mm"
                    ],
                    [
                        "NAME:Hs2",
                        "Value:=", "29.7mm"
                    ],
                    [
                        "NAME:Bs0",
                        "Value:=", "2.11mm"
                    ],
                    [
                        "NAME:Bs1",
                        "Value:=", "3.14mm"
                    ],
                    [
                        "NAME:Bs2",
                        "Value:=", "5.65mm"
                    ],
                    [
                        "NAME:Rs",
                        "Value:=", "2.5mm"
                    ],
                    [
                        "NAME:Layers",
                        "Value:=", "1"
                    ],
                    [
                        "NAME:CoilPitch",
                        "Value:=", "5"
                    ],
                    [
                        "NAME:SpanExt",
                        "Value:=", "18mm"
                    ],
                    [
                        "NAME:BendAngle",
                        "Value:=", "25deg"
                    ],
                    [
                        "NAME:InfoCoil",
                        "Value:=", "1"
                    ]
                ]
            ]
        ])
    oEditor.Rotate(
        [
            "NAME:Selections",
            "Selections:=", "LapCoil1",
            "NewPartsModelFlag:=", "Model"
        ],
        [
            "NAME:RotateParameters",
            "RotateAxis:=", "Z",
            "RotateAngle:=", "7.5deg"
        ])
    oEditor.DuplicateAroundAxis(
        [
            "NAME:Selections",
            "Selections:=", "LapCoil1",
            "NewPartsModelFlag:=", "Model"
        ],
        [
            "NAME:DuplicateAroundAxisParameters",
            "CreateNewObjects:=", True,
            "WhichAxis:=", "Z",
            "AngleStr:=", "15deg",
            "NumClones:=", "3"
        ],
        [
            "NAME:Options",
            "DuplicateAssignments:=", False
        ],
        [
            "CreateGroupsForNewObjects:=", False
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "LapCoil1"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseA"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "LapCoil1_1"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseB"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "LapCoil1_2"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseC"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseB"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Color",
                        "R:=", 0,
                        "G:=", 0,
                        "B:=", 255
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseC"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Color",
                        "R:=", 255,
                        "G:=", 0,
                        "B:=", 0
                    ]
                ]
            ]
        ])
    oEditor.DuplicateAroundAxis(
        [
            "NAME:Selections",
            "Selections:=", "PhaseA,PhaseB,PhaseC",
            "NewPartsModelFlag:=", "Model"
        ],
        [
            "NAME:DuplicateAroundAxisParameters",
            "CreateNewObjects:=", True,
            "WhichAxis:=", "Z",
            "AngleStr:=", "45deg",
            "NumClones:=", "8"
        ],
        [
            "NAME:Options",
            "DuplicateAssignments:=", False
        ],
        [
            "CreateGroupsForNewObjects:=", False
        ])
    print('-----Draw Stator finished.')


def project_setup():
    global oProject, oDesign
    print('-----Project setup finished.')


def output_variables():
    global oProject, oDesign
    print('-----output finished.')


def assign_material():
    global oProject, oDesign, oEditor
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseA"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseA1"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseA_7"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseA_2"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseA1"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseA_1"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseB"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseB_1"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseB_7"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseB_2"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseC"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseC_1"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseC_7"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseC_2"
                    ]
                ]
            ]
        ])
    # oEditor.SeparateBody(
    #     [
    #         "NAME:Selections",
    #         "Selections:=", "Magnets",
    #         "NewPartsModelFlag:=", "Model"
    #     ],
    #     [
    #         "CreateGroupsForNewObjects:=", False
    #     ])
    # oEditor.ChangeProperty(
    #     [
    #         "NAME:AllTabs",
    #         [
    #             "NAME:Geometry3DAttributeTab",
    #             [
    #                 "NAME:PropServers",
    #                 "Magnets"
    #             ],
    #             [
    #                 "NAME:ChangedProps",
    #                 [
    #                     "NAME:Name",
    #                     "Value:=", "PM1"
    #                 ]
    #             ]
    #         ]
    #     ])
    # oEditor.ChangeProperty(
    #     [
    #         "NAME:AllTabs",
    #         [
    #             "NAME:Geometry3DAttributeTab",
    #             [
    #                 "NAME:PropServers",
    #                 "Magnets_Separate1"
    #             ],
    #             [
    #                 "NAME:ChangedProps",
    #                 [
    #                     "NAME:Name",
    #                     "Value:=", "PM2"
    #                 ]
    #             ]
    #         ]
    #     ])

    # oDesign = oProject.SetActiveDesign("ipm2")
    # oEditor = oDesign.SetActiveEditor("3D Modeler")
    # oEditor.ChangeProperty(
    #     [
    #         "NAME:AllTabs",
    #         [
    #             "NAME:Geometry3DAttributeTab",
    #             [
    #                 "NAME:PropServers",
    #                 "PM1"
    #             ],
    #             [
    #                 "NAME:ChangedProps",
    #                 [
    #                     "NAME:Material",
    #                     "Value:=", "\"N36Z_20\""
    #                 ]
    #             ]
    #         ]
    #     ])
    # oEditor.AssignMaterial(
    #     [
    #         "NAME:Selections",
    #         "Selections:=", "PM2"
    #     ],
    #     [
    #         "NAME:Attributes",
    #         "MaterialValue:=", "\"N36Z_20\"",
    #         "SolveInside:=", True,
    #         "IsMaterialEditable:=", True
    #     ])

    oEditor.AssignMaterial(
        [
            "NAME:Selections",
            "Selections:=", "rotor"
        ],
        [
            "NAME:Attributes",
            "MaterialValue:=", "\"M19_29G\"",
            "SolveInside:=", True,
            "IsMaterialEditable:=", True
        ])
    oEditor.AssignMaterial(
        [
            "NAME:Selections",
            "Selections:=", "SlotCore1"
        ],
        [
            "NAME:Attributes",
            "MaterialValue:=", "\"M19_29G\"",
            "SolveInside:=", True,
            "IsMaterialEditable:=", True
        ])
    oEditor.AssignMaterial(
        [
            "NAME:Selections",
            "Selections:=", "magnet1"
        ],
        [
            "NAME:Attributes",
            "MaterialValue:=", "\"N36Z_20_S\"",
            "SolveInside:=", True,
            "IsMaterialEditable:=", True
        ])
    oEditor.AssignMaterial(
        [
            "NAME:Selections",
            "Selections:=", "magnet1_1"
        ],
        [
            "NAME:Attributes",
            "MaterialValue:=", "\"N36Z_20_N\"",
            "SolveInside:=", True,
            "IsMaterialEditable:=", True
        ])
    print('-----assign_material finished.')


def draw_circlrpolygon(o, restPoints):
    theta = []
    radius = []
    points = []
    x = []
    y = []
    r = 0
    sides = len(restPoints)
    for p in restPoints:
        theta.append(float(p.y))

    print
    theta

    c = 0
    theta_sum = sum(theta)
    print
    type(theta_sum)
    for i in range(sides):
        c += theta[i]
        rr = float(restPoints[i].x)
        x = -math.sin(2 * math.pi * c / theta_sum) * rr + float(o.x)
        y = math.cos(2 * math.pi * c / theta_sum) * rr + float(o.y)
        #        x1,y1=pol2cart(radius[i],theta[i])
        points.append(Point(x, y))

    return points


def draw_polygon(points, name, flag="", color="(180 0 0)", transparency=0, partCS="Global", UDMID="",
                 Material="vacuum", SurfaceMaterialValue="", SolveInside=True, IsMaterialEditable=True):
    global oProject, oDesign, unit
    oEditor = oDesign.SetActiveEditor("3D Modeler")
    PolylinePoints = []
    PolylineSegments = []
    PolylinePoints.append("NAME:PolylinePoints")
    for point in points:
        PolylinePoints.append([
            "NAME:PLPoint",
            "X:=", str(point.x) + unit,
            "Y:=", str(point.y) + unit,
            "Z:=", str(point.z) + unit])
    PolylineSegments.append("NAME:PolylineSegments")
    for i in range(len(points)):
        PolylineSegments.append([
            "NAME:PLSegment",
            "SegmentType:=", "Line",
            "StartIndex:=", i,
            "NoOfPoints:=", 2
        ])

    lines = [
        "NAME:PolylineParameters",
        "IsPolylineCovered:=", True,
        "IsPolylineClosed:=", True,
        PolylinePoints,
        PolylineSegments,
        [
            "NAME:PolylineXSection",
            "XSectionType:=", "None",
            "XSectionOrient:=", "Auto",
            "XSectionWidth:=", "0mm",
            "XSectionTopWidth:=", "0mm",
            "XSectionHeight:=", "0mm",
            "XSectionNumSegments:=", "0",
            "XSectionBendType:=", "Corner"
        ]
    ]
    attr = [
        "NAME:Attributes",
        "Name:=", name,
        "Flags:=", flag,
        "Color:=", color,
        "Transparency:=", transparency,
        "PartCoordinateSystem:=", partCS,
        "UDMId:=", UDMID,
        "MaterialValue:=", '\"' + Material + '\"',
        "SurfaceMaterialValue:=", '\"' + SurfaceMaterialValue + '\"',
        "SolveInside:=", SolveInside,
        "IsMaterialEditable:=", IsMaterialEditable
    ]
    # print lines#tested and no errors.
    oEditor.CreatePolyline(lines, attr)
    print('-----draw_polygon finished.')


def draw_rotor(draw_barrier=False):
    # draw a circle first, then generate a number of nodes that representing the magnets and barriers
    draw_circle('rotor', 0, 0, 0, 160.4 / 2, material='M19_29G', color="(0 170 0)", transparency=0.5)
    # System.Windows.Forms.MessageBox.Show("draw circle end", "Warning")
    # print pointFile
    # poly = load_points(pointFile)
    p_magnet_list = []
    p_barrier1_list = []
    p_barrier2_list = []
    # for points in poly.Element('polygons').Element('magnet').Elements():
    #     p = Point()
    #     p.x = points.Element('x').Value
    #     p.y = points.Element('y').Value
    #     p_magnet_list.append(p)
    # if draw_barrier:
    #     for points in poly.Element('polygons').Element('barrier1').Elements():
    #         p = Point()
    #         p.x = points.Element('x').Value
    #         p.y = points.Element('y').Value
    #         p_barrier1_list.append(p)
    #
    #     for points in poly.Element('polygons').Element('barrier2').Elements():
    #         p = Point()
    #         p.x = points.Element('x').Value
    #         p.y = points.Element('y').Value
    #         p_barrier2_list.append(p)

    # print p_magnet_list
    p_magnet_list = p_list
    # print p_barrier1_list
    # print p_barrier2_list
    p_magnet_list_new = draw_circlrpolygon(p_magnet_list[0], p_magnet_list[1:])
    draw_polygon(p_magnet_list_new, 'magnet1')
    str_magnet = 'magnet1'
    str_magnet_d = ',magnet1_1'
    str_barriers = ',barrier1,barrier2'
    str_barriers_d = ',barrier1_1,barrier2_1'
    if draw_barrier:
        draw_polygon(p_barrier1_list, 'barrier1')
        draw_polygon(p_barrier2_list, 'barrier2')
    else:
        str_barriers = ''
        str_barriers_d = ''
    oEditor.DuplicateMirror(
        [
            "NAME:Selections",
            "Selections:=", str_magnet + str_barriers,
            "NewPartsModelFlag:=", "Model"
        ],
        [
            "NAME:DuplicateToMirrorParameters",
            "DuplicateMirrorBaseX:=", "0mm",
            "DuplicateMirrorBaseY:=", "0mm",
            "DuplicateMirrorBaseZ:=", "0mm",
            "DuplicateMirrorNormalX:=", "0.707106781186547mm",
            "DuplicateMirrorNormalY:=", "-0.707106781186547mm",
            "DuplicateMirrorNormalZ:=", "0mm"
        ],
        [
            "NAME:Options",
            "DuplicateAssignments:=", False
        ],
        [
            "CreateGroupsForNewObjects:=", False
        ])

    oEditor.Rotate(
        [
            "NAME:Selections",
            "Selections:=", str_magnet_d + str_barriers_d,
            "NewPartsModelFlag:=", "Model"
        ],
        [
            "NAME:RotateParameters",
            "RotateAxis:=", "Z",
            "RotateAngle:=", "-45deg"
        ])
    oEditor.Subtract(
        [
            "NAME:Selections",
            "Blank Parts:=", "rotor",
            "Tool Parts:=", str_magnet + str_magnet_d + str_barriers + str_barriers_d
        ],
        [
            "NAME:SubtractParameters",
            "KeepOriginals:=", True
        ])
    print('-----draw_rotor finished.')


def draw_circle(name, x, y, z, radius, segments=0, unit='mm', color="(143 175 143)", transparency=0, material="vacuum"):
    oEditor.CreateCircle(
        [
            "NAME:CircleParameters",
            "IsCovered:=", True,
            "XCenter:=", str(x) + unit,
            "YCenter:=", str(y) + unit,
            "ZCenter:=", str(z) + unit,
            "Radius:=", str(radius) + unit,
            "WhichAxis:=", "Z",
            "NumSegments:=", str(segments)
        ],
        [
            "NAME:Attributes",
            "Name:=", str(name),
            "Flags:=", "",
            "Color:=", color,
            "Transparency:=", transparency,
            "PartCoordinateSystem:=", "Global",
            "UDMId:=", "",
            "MaterialValue:=", '\"' + material + '\"',
            "SurfaceMaterialValue:=", "\"\"",
            "SolveInside:=", True,
            "IsMaterialEditable:=", True
        ])
    print('-----draw_circle finished.')


def setup_winding():
    global oEditor
    oEditor.Split(
        [
            "NAME:Selections",
            "Selections:=",
            "SlotCore1,PhaseA,PhaseB,PhaseC,PhaseA_1,PhaseA_2,PhaseA_3,PhaseA_4,PhaseA_5,PhaseA_6,PhaseA_7,PhaseB_1,PhaseB_2,PhaseB_3,PhaseB_4,PhaseB_5,PhaseB_6,PhaseB_7,PhaseC_1,PhaseC_2,PhaseC_3,PhaseC_4,PhaseC_5,PhaseC_6,PhaseC_7,rotor",
            "NewPartsModelFlag:=", "Model"
        ],
        [
            "NAME:SplitToParameters",
            "SplitPlane:=", "ZX",
            "WhichSide:=", "PositiveOnly",
            "SplitCrossingObjectsOnly:=", False,
            "DeleteInvalidObjects:=", True
        ])
    oEditor.Rotate(
        [
            "NAME:Selections",
            "Selections:=",
            "SlotCore1,PhaseA,PhaseB,PhaseC,PhaseA_1,PhaseA_2,PhaseA_3,PhaseA_7,PhaseB_1,PhaseB_2,PhaseB_3,PhaseB_7,PhaseC_1,PhaseC_2,PhaseC_3,PhaseC_7,rotor",
            "NewPartsModelFlag:=", "Model"
        ],
        [
            "NAME:RotateParameters",
            "RotateAxis:=", "Z",
            "RotateAngle:=", "-45deg"
        ])
    oEditor.Split(
        [
            "NAME:Selections",
            "Selections:=",
            "SlotCore1,PhaseA,PhaseB,PhaseC,PhaseA_1,PhaseA_2,PhaseA_3,PhaseA_7,PhaseB_1,PhaseB_2,PhaseB_3,PhaseB_7,PhaseC_1,PhaseC_2,PhaseC_3,PhaseC_7,rotor",
            "NewPartsModelFlag:=", "Model"
        ],
        [
            "NAME:SplitToParameters",
            "SplitPlane:=", "ZX",
            "WhichSide:=", "NegativeOnly",
            "SplitCrossingObjectsOnly:=", False,
            "DeleteInvalidObjects:=", True
        ])
    oEditor.Rotate(
        [
            "NAME:Selections",
            "Selections:=", "SlotCore1,PhaseA,PhaseB,PhaseC,PhaseA_7,PhaseB_7,PhaseC_7,rotor",
            "NewPartsModelFlag:=", "Model"
        ],
        [
            "NAME:RotateParameters",
            "RotateAxis:=", "Z",
            "RotateAngle:=", "45deg"
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseA"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseA1"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseA_7"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseA_2"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseA1"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseA_1"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseB"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseB_1"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseB_7"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseB_2"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseC"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseC_1"
                    ]
                ]
            ]
        ])
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DAttributeTab",
                [
                    "NAME:PropServers",
                    "PhaseC_7"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Name",
                        "Value:=", "PhaseC_2"
                    ]
                ]
            ]
        ])
    print('-----setup_winding finished.')


def setup_CS():
    # oEditor.CreateFaceCS(
    #     [
    #         "NAME:FaceCSParameters",
    #         "PartID:="		, 40318,
    #         [
    #             "NAME:Origin",
    #             "IsAttachedToEntity:="	, True,
    #             "EntityID:="		, 40687,
    #             "PositionType:="	, "OnVertex",
    #             "UParam:="		, 0,
    #             "VParam:="		, 0,
    #             "XPosition:="		, "0",
    #             "YPosition:="		, "0",
    #             "ZPosition:="		, "0"
    #         ],
    #         "MoveToEnd:="		, False,
    #         "FaceID:="		, 40586,
    #         [
    #             "NAME:AxisPosn",
    #             "IsAttachedToEntity:="	, True,
    #             "EntityID:="		, 40684,
    #             "PositionType:="	, "OnVertex",
    #             "UParam:="		, 0,
    #             "VParam:="		, 0,
    #             "XPosition:="		, "0",
    #             "YPosition:="		, "0",
    #             "ZPosition:="		, "0"
    #         ],
    #         "WhichAxis:="		, "X"
    #     ],
    #     [
    #         "NAME:Attributes",
    #         "Name:="		, "FaceCS1"
    #     ])
    # oEditor.ChangeProperty(
    #     [
    #         "NAME:AllTabs",
    #         [
    #             "NAME:Geometry3DCSTab",
    #             [
    #                 "NAME:PropServers",
    #                 "FaceCS1"
    #             ],
    #             [
    #                 "NAME:ChangedProps",
    #                 [
    #                     "NAME:Name",
    #                     "Value:="		, "PM1_CS"
    #                 ]
    #             ]
    #         ]
    #     ])
    # oEditor.CreateFaceCS(
    #     [
    #         "NAME:FaceCSParameters",
    #         "PartID:="		, 40902,
    #         [
    #             "NAME:Origin",
    #             "IsAttachedToEntity:="	, True,
    #             "EntityID:="		, 40721,
    #             "PositionType:="	, "OnVertex",
    #             "UParam:="		, 0,
    #             "VParam:="		, 0,
    #             "XPosition:="		, "0",
    #             "YPosition:="		, "0",
    #             "ZPosition:="		, "0"
    #         ],
    #         "MoveToEnd:="		, False,
    #         "FaceID:="		, 40595,
    #         [
    #             "NAME:AxisPosn",
    #             "IsAttachedToEntity:="	, True,
    #             "EntityID:="		, 40722,
    #             "PositionType:="	, "OnVertex",
    #             "UParam:="		, 0,
    #             "VParam:="		, 0,
    #             "XPosition:="		, "0",
    #             "YPosition:="		, "0",
    #             "ZPosition:="		, "0"
    #         ],
    #         "WhichAxis:="		, "X"
    #     ],
    #     [
    #         "NAME:Attributes",
    #         "Name:="		, "FaceCS1"
    #     ])
    # oEditor.ChangeProperty(
    #     [
    #         "NAME:AllTabs",
    #         [
    #             "NAME:Geometry3DCSTab",
    #             [
    #                 "NAME:PropServers",
    #                 "FaceCS1"
    #             ],
    #             [
    #                 "NAME:ChangedProps",
    #                 [
    #                     "NAME:Name",
    #                     "Value:="		, "PM2_CS"
    #                 ]
    #             ]
    #         ]
    #     ])
    # oEditor.SetWCS(
    #     [
    #         "NAME:SetWCS Parameter",
    #         "Working Coordinate System:=", "Global",
    #         "RegionDepCSOk:="	, False
    #     ])
    # oEditor.ChangeProperty(
    #     [
    #         "NAME:AllTabs",
    #         [
    #             "NAME:Geometry3DAttributeTab",
    #             [
    #                 "NAME:PropServers",
    #                 "PM1"
    #             ],
    #             [
    #                 "NAME:ChangedProps",
    #                 [
    #                     "NAME:Orientation",
    #                     "Value:="		, "PM1_CS"
    #                 ]
    #             ]
    #         ]
    #     ])
    # oEditor.ChangeProperty(
    #     [
    #         "NAME:AllTabs",
    #         [
    #             "NAME:Geometry3DAttributeTab",
    #             [
    #                 "NAME:PropServers",
    #                 "PM2"
    #             ],
    #             [
    #                 "NAME:ChangedProps",
    #                 [
    #                     "NAME:Orientation",
    #                     "Value:="		, "PM2_CS"
    #                 ]
    #             ]
    #         ]
    #     ])
    print('-----setup_CS finished')


def draw_wedge(name, x, y, z, radius, deg, segments=0):
    global oDesign, oEditor, unit
    oEditor.CreatePolyline(
        [
            "NAME:PolylineParameters",
            "IsPolylineCovered:=", True,
            "IsPolylineClosed:=", False,
            [
                "NAME:PolylinePoints",
                [
                    "NAME:PLPoint",
                    "X:=", str(x) + unit,
                    "Y:=", str(y) + unit,
                    "Z:=", str(z) + unit
                ],
                [
                    "NAME:PLPoint",
                    "X:=", str(radius) + unit,
                    "Y:=", str(y) + unit,
                    "Z:=", str(z) + unit
                ]
            ],
            [
                "NAME:PolylineSegments",
                [
                    "NAME:PLSegment",
                    "SegmentType:=", "Line",
                    "StartIndex:=", 0,
                    "NoOfPoints:=", 2
                ]
            ],
            [
                "NAME:PolylineXSection", "XSectionType:=", "None",
                "XSectionOrient:=", "Auto",
                "XSectionWidth:=", "0mm",
                "XSectionTopWidth:=", "0mm",
                "XSectionHeight:=", "0mm",
                "XSectionNumSegments:=", "0",
                "XSectionBendType:=", "Corner"
            ]
        ],
        [
            "NAME:Attributes",
            "Name:=", name,
            "Flags:=", "",
            "Color:=", "(143 175 143)",
            "Transparency:=", 0.8,
            "PartCoordinateSystem:=", "Global",
            "UDMId:=", "",
            "MaterialValue:=", "\"vacuum\"",
            "SurfaceMaterialValue:=", "\"\"",
            "SolveInside:=", True,
            "IsMaterialEditable:=", True
        ])
    oEditor.SweepAroundAxis(
        [
            "NAME:Selections",
            "Selections:=", name,
            "NewPartsModelFlag:=", "Model"
        ],
        [
            "NAME:AxisSweepParameters",
            "DraftAngle:=", "0deg",
            "DraftType:=", "Round",
            "CheckFaceFaceIntersection:=", False,
            "SweepAxis:=", "Z",
            "SweepAngle:=", str(deg) + "deg",
            "NumOfSegments:=", str(segments)
        ])


def coil_setuo():
    print('-----coil_setup finished')


def boundary_setup():
    global oDesign, oEditor, working_mode
    if working_mode == 'partial':
        draw_wedge('outband', 0, 0, 0, radius=200, deg=45, segments=5)
    else:
        draw_circle('outband', 0, 0, 0, radius=200, transparency=0.8)
    oModule = oDesign.GetModule("BoundarySetup")
    oModule.AssignCurrentGroup(["PhaseA1_1", "PhaseA1_2", "PhaseA1_3", "PhaseA1_4", "PhaseA1_5", "PhaseA1_6"],
                               [
                                   "NAME:PhaseA1_1",
                                   "Objects:=",
                                   ["PhaseA_1", "PhaseA_2", "PhaseB_1", "PhaseB_2", "PhaseC_1", "PhaseC_2"],
                                   "Current:=", "1500A",
                                   "IsSolid:=", True,
                                   "IsPositive:=", True
                               ])
    oModule.EditCurrent("PhaseA1_2",
                        [
                            "NAME:PhaseA2",
                            "Current:=", "1500A",
                            "IsSolid:=", True,
                            "IsPositive:=", True
                        ])
    oModule.EditCurrent("PhaseA1_1",
                        [
                            "NAME:PhaseA1",
                            "Current:=", "1500A",
                            "IsSolid:=", True,
                            "IsPositive:=", True
                        ])
    oModule.EditCurrent("PhaseA1_5",
                        [
                            "NAME:PhaseC1",
                            "Current:=", "-750A",
                            "IsSolid:=", True,
                            "IsPositive:=", False
                        ])
    oModule.AssignCoilGroup(["PhaseA_1", "PhaseA_2", "PhaseB_1", "PhaseB_2", "PhaseC_1", "PhaseC_2"],
                            [
                                "NAME:PhaseA_1",
                                "Objects:=", ["PhaseA_1", "PhaseA_2", "PhaseB_1", "PhaseB_2", "PhaseC_1", "PhaseC_2"],
                                "Conductor number:=", "9",
                                "PolarityType:=", "Positive"
                            ])
    oModule.DeleteBoundaries(["PhaseC1"])
    oModule.DeleteBoundaries(["PhaseA2"])
    oModule.DeleteBoundaries(["PhaseA1_6"])
    oModule.DeleteBoundaries(["PhaseA1_4"])
    oModule.DeleteBoundaries(["PhaseA1_3"])
    oModule.DeleteBoundaries(["PhaseA1"])
    oModule.EditCoil("PhaseC_2",
                     [
                         "NAME:PhaseC_2",
                         "Conductor number:=", "9",
                         "PolarityType:=", "Negative"
                     ])
    oModule.EditCoil("PhaseC_1",
                     [
                         "NAME:PhaseC_1",
                         "Conductor number:=", "9",
                         "PolarityType:=", "Negative"
                     ])
    oDesign.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:LocalVariableTab",
                [
                    "NAME:PropServers",
                    "LocalVariables"
                ],
                [
                    "NAME:NewProps",
                    [
                        "NAME:Poles",
                        "PropType:=", "VariableProp",
                        "UserDef:=", True,
                        "Value:=", "8"
                    ],
                    [
                        "NAME:PolePairs",
                        "PropType:=", "VariableProp",
                        "UserDef:=", True,
                        "Value:=", "Poles/2"
                    ],
                    [
                        "NAME:Speed_rpm",
                        "PropType:=", "VariableProp",
                        "UserDef:=", True,
                        "Value:=", "3000"
                    ],
                    [
                        "NAME:Omega",
                        "PropType:=", "VariableProp",
                        "UserDef:=", True,
                        "Value:=", "360*Speed_rpm*PolePairs/60"
                    ],
                    [
                        "NAME:Omega_rad",
                        "PropType:=", "VariableProp",
                        "UserDef:=", True,
                        "Value:=", "Omega * pi / 180"
                    ],
                    [
                        "NAME:Thet_deg",
                        "PropType:=", "VariableProp",
                        "UserDef:=", True,
                        "Value:=", "20"
                    ],
                    [
                        "NAME:Thet",
                        "PropType:=", "VariableProp",
                        "UserDef:=", True,
                        "Value:=", "Thet_deg * pi /180"
                    ],
                    [
                        "NAME:Imax",
                        "PropType:=", "VariableProp",
                        "UserDef:=", True,
                        "Value:=", "250A"
                    ]
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:PolePairs",
                        "Value:=", "Poles/2"
                    ],
                    [
                        "NAME:Imax",
                        "Value:=", "250A"
                    ]
                ]
            ]
        ])
    oModule.AssignWindingGroup(
        [
            "NAME:PhaseA",
            "Type:=", "Current",
            "IsSolid:=", False,
            "Current:=", "Imax*sin(Omega_rad*Time+Thet)",
            "Resistance:=", "0ohm",
            "Inductance:=", "0nH",
            "Voltage:=", "0mV",
            "ParallelBranchesNum:=", "1"
        ])
    oModule.AddCoilstoWinding(
        [
            "NAME:AddTerminalsToWinding",
            [
                "NAME:BoundaryList",
                [
                    "NAME:PhaseA_1",
                    "Objects:=", ["PhaseA_1"],
                    "ParentBndID:=", "PhaseA",
                    "Conductor number:=", "9",
                    "Winding:=", "PhaseA",
                    "PolarityType:=", "Negative"
                ],
                [
                    "NAME:PhaseA_2",
                    "Objects:=", ["PhaseA_2"],
                    "ParentBndID:=", "PhaseA",
                    "Conductor number:=", "9",
                    "Winding:=", "PhaseA",
                    "PolarityType:=", "Negative"
                ]
            ]
        ])
    oModule.AssignWindingGroup(
        [
            "NAME:PhaseB",
            "Type:=", "Current",
            "IsSolid:=", False,
            "Current:=", "Imax*sin(Omega_rad*Time+2*pi/3+Thet)",
            "Resistance:=", "0ohm",
            "Inductance:=", "0nH",
            "Voltage:=", "0mV",
            "ParallelBranchesNum:=", "1"
        ])
    oModule.AssignWindingGroup(
        [
            "NAME:PhaseC",
            "Type:=", "Current",
            "IsSolid:=", False,
            "Current:=", "Imax*sin(Omega_rad*Time-2*pi/3+Thet)",
            "Resistance:=", "0ohm",
            "Inductance:=", "0nH",
            "Voltage:=", "0mV",
            "ParallelBranchesNum:=", "1"
        ])
    oModule.AddCoilstoWinding(
        [
            "NAME:AddTerminalsToWinding",
            [
                "NAME:BoundaryList",
                [
                    "NAME:PhaseB_1",
                    "Objects:=", ["PhaseB_1"],
                    "ParentBndID:=", "PhaseB",
                    "Conductor number:=", "9",
                    "Winding:=", "PhaseB",
                    "PolarityType:=", "Positive"
                ],
                [
                    "NAME:PhaseB_2",
                    "Objects:=", ["PhaseB_2"],
                    "ParentBndID:=", "PhaseB",
                    "Conductor number:=", "9",
                    "Winding:=", "PhaseB",
                    "PolarityType:=", "Positive"
                ]
            ]
        ])
    oModule.AddCoilstoWinding(
        [
            "NAME:AddTerminalsToWinding",
            [
                "NAME:BoundaryList",
                [
                    "NAME:PhaseC_1",
                    "Objects:=", ["PhaseC_1"],
                    "ParentBndID:=", "PhaseC",
                    "Conductor number:=", "9",
                    "Winding:=", "PhaseC",
                    "PolarityType:=", "Negative"
                ],
                [
                    "NAME:PhaseC_2",
                    "Objects:=", ["PhaseC_2"],
                    "ParentBndID:=", "PhaseC",
                    "Conductor number:=", "9",
                    "Winding:=", "PhaseC",
                    "PolarityType:=", "Negative"
                ]
            ]
        ])
    oModule.SetCoreLoss(["SlotCore1", "rotor"], False)
    if working_mode == 'partial':
        draw_wedge('band', 0, 0, 0, 80.575, 45, 0)
    else:
        draw_circle('band', 0, 0, 0, radius=80.575, transparency=0.8)

    edgeList = oEditor.GetEdgeIDsFromObject('outband')
    # print 'edgeList is below'
    # print edgeList
    ##edge list is a list of string, so elements inside should be converted to integer
    edgeList = map(int, edgeList)  # in python2
    # print edgeList
    oModule = oDesign.GetModule("BoundarySetup")
    oModule.AssignMaster(
        [
            "NAME:Master1",
            "Edges:=", [edgeList[-1]],
            "ReverseV:=", False
        ])
    oModule.AssignSlave(
        [
            "NAME:Slave1",
            "Edges:=", [edgeList[-2]],
            "ReverseU:=", True,
            "Master:=", "Master1",
            "SameAsMaster:=", False
        ])
    oModule.AssignVectorPotential(
        [
            "NAME:zeroflux",
            "Edges:=", edgeList[:-2],
            "Value:=", "0",
            "CoordinateSystem:=", ""
        ])
    print('-----BoundarySetup finished')


def winding_setup():
    print('-----winding_setup finished')


def analysis_setup():
    global oDesign
    oModule = oDesign.GetModule("MeshSetup")
    oModule.AssignLengthOp(
        [
            "NAME:Length2",
            "RefineInside:=", False,
            "Enabled:=", True,
            "Objects:=",
            ["outband", "SlotCore1", "PhaseA_1", "PhaseB_1", "PhaseC_1", "PhaseA_2", "PhaseB_2", "PhaseC_2", "rotor",
             "magnet1", "magnet1_1", "band"],
            "RestrictElem:=", False,
            "NumMaxElem:=", "2000",
            "RestrictLength:=", True,
            "MaxLength:=", "2mm"
        ])
    oModule = oDesign.GetModule("AnalysisSetup")
    oModule.InsertSetup("Transient",
                        [
                            "NAME:Setup1",
                            "Enabled:=", True,
                            "NonlinearSolverResidual:=", "0.0001",
                            "TimeIntegrationMethod:=", 0,
                            "StopTime:=", "15ms",
                            "TimeStep:=", "250us",
                            "OutputError:=", False,
                            "UseControlProgram:=", False,
                            "ControlProgramName:=", " ",
                            "ControlProgramArg:=", " ",
                            "CallCtrlProgAfterLastStep:=", False,
                            "FastReachSteadyState:=", False,
                            "IsGeneralTransient:=", True,
                            "HasSweepSetup:=", False,
                            "UseAdaptiveTimeStep:=", False,
                            "InitialTimeStep:=", "0.002s",
                            "MinTimeStep:=", "0.001s",
                            "MaxTimeStep:=", "0.003s",
                            "TimeStepErrTolerance:=", 0.0001
                        ])
    print('-----analysis_setup finished')


def output_setup(filename):
    table_name = "Output Variables Plot 1"
    oModule = oDesign.GetModule("ReportSetup")
    oModule.CreateReport("Torque Plot 1", "Transient", "Rectangular Plot", "Setup1 : Transient",
                         [
                             "Domain:=", "Sweep"
                         ],
                         [
                             "Time:=", ["All"],
                             "Poles:=", ["Nominal"],
                             "Speed_rpm:=", ["Nominal"],
                             "Thet_deg:=", ["Nominal"],
                             "Imax:=", ["Nominal"]
                         ],
                         [
                             "X Component:=", "Time",
                             "Y Component:=", ["Moving1.Torque"]
                         ], [])
    oModule = oDesign.GetModule("OutputVariable")
    oModule.CreateOutputVariable("t1", "Moving1.Torque", "Setup1 : Transient", "Transient", [])
    oModule = oDesign.GetModule("ReportSetup")
    oModule.CreateReport(table_name, "Transient", "Rectangular Plot", "Setup1 : Transient",
                         [
                             "Domain:=", "Sweep"
                         ],
                         [
                             "Time:=", ["All"],
                             "Poles:=", ["Nominal"],
                             "Speed_rpm:=", ["Nominal"],
                             "Thet_deg:=", ["Nominal"],
                             "Imax:=", ["Nominal"]
                         ],
                         [
                             "X Component:=", "Poles",
                             "Y Component:=", ["mean(t1)"]
                         ], [])
    pathlist = os.path.split((os.path.abspath('ipm_' + outfile + '.py')))
    # print pathlist
    output_name = pathlist[1].split('.')
    output_file = pathlist[0] + '\\vars\\' + output_name[0] + ".csv"
    oModule.ExportToFile(table_name, output_file)
    print('-----output_setup finished')


def model_setup():
    global oDesign
    oModule = oDesign.GetModule("ModelSetup")
    oModule.AssignBand(
        [
            "NAME:Data",
            "Move Type:=", "Rotate",
            "Coordinate System:=", "Global",
            "Axis:=", "Z",
            "Is Positive:=", True,
            "InitPos:=", "30deg",
            "HasRotateLimit:=", False,
            "NonCylindrical:=", False,
            "Consider Mechanical Transient:=", False,
            "Angular Velocity:=", "3000rpm",
            "Objects:=", ["band"]
        ])
    oDesign.SetDesignSettings(
        [
            "NAME:Design Settings Data",
            "Perform Minimal validation:=", False,
            "EnabledObjects:=", [],
            "PreserveTranSolnAfterDatasetEdit:=", False,
            "ComputeTransientInductance:=", False,
            "ComputeIncrementalMatrix:=", False,
            "PerfectConductorThreshold:=", 1E+030,
            "InsulatorThreshold:=", 1,
            "ModelDepth:=", "83.82mm",
            "UseSkewModel:=", False,
            "EnableTranTranLinkWithSimplorer:=", False,
            "BackgroundMaterialName:=", "vacuum",
            "Multiplier:=", "8"
        ],
        [
            "NAME:Model Validation Settings",
            "EntityCheckLevel:=", "Strict",
            "IgnoreUnclassifiedObjects:=", False,
            "SkipIntersectionChecks:=", False
        ])
    print('-----model_setup finished')


# points=[ ]
# points.append(Point(0,0 ,0 ))
# points.append(Point(0,5 ,0 ))
# points.append(Point(5,5 ,0 ))
# points.append(Point(5,0 ,0 ))
#
# draw_polygon(points, "square1")
# filename = "e:\ipm.aedt"
#
# oProject.SaveAs(filename, True)
def split_model():
    try:
        global oDesign, oEditor
        oEditor.Split(
            [
                "NAME:Selections",
                "Selections:=",
                "SlotCore1,PhaseA,PhaseB,PhaseC,PhaseA_1,PhaseA_2,PhaseA_3,PhaseA_4,PhaseA_5,PhaseA_6,PhaseA_7,PhaseB_1,PhaseB_2,PhaseB_3,PhaseB_4,PhaseB_5,PhaseB_6,PhaseB_7,PhaseC_1,PhaseC_2,PhaseC_3,PhaseC_4,PhaseC_5,PhaseC_6,PhaseC_7,rotor",
                "NewPartsModelFlag:=", "Model"
            ],
            [
                "NAME:SplitToParameters",
                "SplitPlane:=", "ZX",
                "WhichSide:=", "PositiveOnly",
                "SplitCrossingObjectsOnly:=", False,
                "DeleteInvalidObjects:=", True
            ])
        oEditor.Rotate(
            [
                "NAME:Selections",
                "Selections:=",
                "SlotCore1,PhaseA,PhaseB,PhaseC,PhaseA_1,PhaseA_2,PhaseA_3,PhaseA_7,PhaseB_1,PhaseB_2,PhaseB_3,PhaseB_7,PhaseC_1,PhaseC_2,PhaseC_3,PhaseC_7,rotor",
                "NewPartsModelFlag:=", "Model"
            ],
            [
                "NAME:RotateParameters",
                "RotateAxis:=", "Z",
                "RotateAngle:=", "-45deg"
            ])
        oEditor.Split(
            [
                "NAME:Selections",
                "Selections:=",
                "SlotCore1,PhaseA,PhaseB,PhaseC,PhaseA_1,PhaseA_2,PhaseA_3,PhaseA_7,PhaseB_1,PhaseB_2,PhaseB_3,PhaseB_7,PhaseC_1,PhaseC_2,PhaseC_3,PhaseC_7,rotor",
                "NewPartsModelFlag:=", "Model"
            ],
            [
                "NAME:SplitToParameters",
                "SplitPlane:=", "ZX",
                "WhichSide:=", "NegativeOnly",
                "SplitCrossingObjectsOnly:=", False,
                "DeleteInvalidObjects:=", True
            ])
        oEditor.Rotate(
            [
                "NAME:Selections",
                "Selections:=", "SlotCore1,PhaseA,PhaseB,PhaseC,PhaseA_7,PhaseB_7,PhaseC_7,rotor",
                "NewPartsModelFlag:=", "Model"
            ],
            [
                "NAME:RotateParameters",
                "RotateAxis:=", "Z",
                "RotateAngle:=", "45deg"
            ])
    except:
        print('-----Error and application quit')
        oDesktop.QuitApplication()


def add_material():
    global oProject
    oDefinitionManager = oProject.GetDefinitionManager()
    oDefinitionManager.AddMaterial(
        [
            "NAME:N36Z_20_N",
            "CoordinateSystemType:=", "Cylindrical",
            "BulkOrSurfaceType:=", 1,
            [
                "NAME:PhysicsTypes",
                "set:=", ["Electromagnetic"]
            ], "permeability:=", "1.03",
            [
                "NAME:magnetic_coercivity",
                "property_type:=", "VectorProperty",
                "Magnitude:=", "-920000A_per_meter",
                "DirComp1:=", "1",
                "DirComp2:=", "0",
                "DirComp3:=", "0"
            ]
        ])
    oDefinitionManager.AddMaterial(
        [
            "NAME:N36Z_20_S",
            "CoordinateSystemType:=", "Cylindrical",
            "BulkOrSurfaceType:=", 1,
            [
                "NAME:PhysicsTypes",
                "set:=", ["Electromagnetic"]
            ], "permeability:=", "1.03",
            [
                "NAME:magnetic_coercivity",
                "property_type:=", "VectorProperty",
                "Magnitude:=", "-920000A_per_meter",
                "DirComp1:=", "-1",
                "DirComp2:=", "0",
                "DirComp3:=", "0"
            ]
        ])
    oDefinitionManager.AddMaterial(
        [
            "NAME:M19_29G",
            "CoordinateSystemType:=", "Cartesian",
            "BulkOrSurfaceType:=", 1,
            [
                "NAME:PhysicsTypes",
                "set:=", ["Electromagnetic"]
            ],
            [
                "NAME:AttachedData",
                [
                    "NAME:CoreLossMultiCurveData",
                    "property_data:=", "coreloss_multi_curve_data",
                    "coreloss_unit:=", "w_per_kg",
                    [
                        "NAME:AllCurves",
                        [
                            "NAME:OneCurve",
                            "Frequency:=", "50Hz",
                            [
                                "NAME:Coordinates",
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0,
                                    "Y:=", 0
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.1,
                                    "Y:=", 0.03
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.2,
                                    "Y:=", 0.07
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.3,
                                    "Y:=", 0.13
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.4,
                                    "Y:=", 0.22
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.5,
                                    "Y:=", 0.31
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.6,
                                    "Y:=", 0.43
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.7,
                                    "Y:=", 0.54
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.8,
                                    "Y:=", 0.68
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.9,
                                    "Y:=", 0.83
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1,
                                    "Y:=", 1.01
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.1,
                                    "Y:=", 1.2
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.2,
                                    "Y:=", 1.42
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.3,
                                    "Y:=", 1.7
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.4,
                                    "Y:=", 2.12
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.5,
                                    "Y:=", 2.47
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.6,
                                    "Y:=", 2.8
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.7,
                                    "Y:=", 3.05
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.8,
                                    "Y:=", 3.25
                                ]
                            ]
                        ],
                        [
                            "NAME:OneCurve",
                            "Frequency:=", "100Hz",
                            [
                                "NAME:Coordinates",
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0,
                                    "Y:=", 0
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.1,
                                    "Y:=", 0.04
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.2,
                                    "Y:=", 0.16
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.3,
                                    "Y:=", 0.34
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.4,
                                    "Y:=", 0.55
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.5,
                                    "Y:=", 0.8
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.6,
                                    "Y:=", 1.08
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.7,
                                    "Y:=", 1.38
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.8,
                                    "Y:=", 1.73
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.9,
                                    "Y:=", 2.1
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1,
                                    "Y:=", 2.51
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.1,
                                    "Y:=", 2.98
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.2,
                                    "Y:=", 3.51
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.3,
                                    "Y:=", 4.15
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.4,
                                    "Y:=", 4.97
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.5,
                                    "Y:=", 5.92
                                ]
                            ]
                        ],
                        [
                            "NAME:OneCurve",
                            "Frequency:=", "200Hz",
                            [
                                "NAME:Coordinates",
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0,
                                    "Y:=", 0
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.1,
                                    "Y:=", 0.09
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.2,
                                    "Y:=", 0.37
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.3,
                                    "Y:=", 0.79
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.4,
                                    "Y:=", 1.31
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.5,
                                    "Y:=", 1.91
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.6,
                                    "Y:=", 2.61
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.7,
                                    "Y:=", 3.39
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.8,
                                    "Y:=", 4.26
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.9,
                                    "Y:=", 5.23
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1,
                                    "Y:=", 6.3
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.1,
                                    "Y:=", 7.51
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.2,
                                    "Y:=", 8.88
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.3,
                                    "Y:=", 10.5
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.4,
                                    "Y:=", 12.5
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.5,
                                    "Y:=", 14.9
                                ]
                            ]
                        ],
                        [
                            "NAME:OneCurve",
                            "Frequency:=", "400Hz",
                            [
                                "NAME:Coordinates",
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0,
                                    "Y:=", 0
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.1,
                                    "Y:=", 0.21
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.2,
                                    "Y:=", 0.92
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.3,
                                    "Y:=", 1.99
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.4,
                                    "Y:=", 3.33
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.5,
                                    "Y:=", 4.94
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.6,
                                    "Y:=", 6.84
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.7,
                                    "Y:=", 9
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.8,
                                    "Y:=", 11.4
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.9,
                                    "Y:=", 14.2
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1,
                                    "Y:=", 17.3
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.1,
                                    "Y:=", 20.9
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.2,
                                    "Y:=", 24.9
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.3,
                                    "Y:=", 29.5
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.4,
                                    "Y:=", 35.4
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.5,
                                    "Y:=", 41.8
                                ]
                            ]
                        ],
                        [
                            "NAME:OneCurve",
                            "Frequency:=", "1000Hz",
                            [
                                "NAME:Coordinates",
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0,
                                    "Y:=", 0
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.1,
                                    "Y:=", 0.99
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.2,
                                    "Y:=", 3.67
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.3,
                                    "Y:=", 7.63
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.4,
                                    "Y:=", 12.7
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.5,
                                    "Y:=", 18.9
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.6,
                                    "Y:=", 26.4
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.7,
                                    "Y:=", 35.4
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.8,
                                    "Y:=", 46
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 0.9,
                                    "Y:=", 58.4
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1,
                                    "Y:=", 73
                                ],
                                [
                                    "NAME:Coordinate",
                                    "X:=", 1.1,
                                    "Y:=", 90.1
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            [
                "NAME:permeability", "property_type:=", "nonlinear",
                "BType:=", "normal",
                "HUnit:=", "A_per_meter",
                "BUnit:=", "tesla",
                [
                    "NAME:BHCoordinates",
                    [
                        "NAME:Coordinate",
                        "X:=", 0,
                        "Y:=", 0
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 22.28,
                        "Y:=", 0.05
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 25.46,
                        "Y:=", 0.1
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 31.83,
                        "Y:=", 0.15
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 47.74,
                        "Y:=", 0.36
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 63.66,
                        "Y:=", 0.54
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 79.57,
                        "Y:=", 0.65
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 159.15,
                        "Y:=", 0.99
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 318.3,
                        "Y:=", 1.2
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 477.46,
                        "Y:=", 1.28
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 636.61,
                        "Y:=", 1.33
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 795.77,
                        "Y:=", 1.36
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 1591.5,
                        "Y:=", 1.44
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 3183,
                        "Y:=", 1.52
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 4774.6,
                        "Y:=", 1.58
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 6366.1,
                        "Y:=", 1.63
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 7957.7,
                        "Y:=", 1.67
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 15915,
                        "Y:=", 1.8
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 31830,
                        "Y:=", 1.9
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 111407,
                        "Y:=", 2
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 190984,
                        "Y:=", 2.1
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 350138,
                        "Y:=", 2.3
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 509252,
                        "Y:=", 2.5
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 560177.2,
                        "Y:=", 2.563994494
                    ],
                    [
                        "NAME:Coordinate",
                        "X:=", 1527756,
                        "Y:=", 3.779889874
                    ]
                ]
            ],
            [
                "NAME:magnetic_coercivity",
                "property_type:=", "VectorProperty",
                "Magnitude:=", "0A_per_meter",
                "DirComp1:=", "1",
                "DirComp2:=", "0",
                "DirComp3:=", "0"
            ],
            [
                "NAME:core_loss_type",
                "property_type:=", "ChoiceProperty",
                "Choice:=", "Electrical Steel"
            ],
            "core_loss_kh:=", "184.233670546732",
            "core_loss_kc:=", "0.386260592696451",
            "core_loss_ke:=", "0.270231418332487",
            "core_loss_kdc:=", "0",
            "mass_density:=", "7872"
        ])


def start_analysis():
    print('-----start analysis')


def main():
    global oDesktop
    try:

        add_material()
        draw_stator()
        draw_rotor()
        if working_mode == 'partial':
            split_model()
        assign_material()
        setup_CS()
        boundary_setup()
        model_setup()
        analysis_setup()
        oDesign.AnalyzeAll()
        output_setup(outfile)
        oProject.Save()
        return
    except:
        print('-----Error and application quit')
        return


if __name__ == '__main__':
    main()
    oDesktop.QuitApplication()
