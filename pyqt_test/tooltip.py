import sys
import random
from PyQt5.QtWidgets import (QMainWindow, QWidget, QAction, qApp, QToolTip,
                             QDesktopWidget,
                             QPushButton, QApplication, QMessageBox, QMenu,
                             QLabel, QLineEdit,
                             QTextEdit, QGridLayout, QSizePolicy, QVBoxLayout
                             )
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT
from matplotlib.figure import Figure

from PyQt5.QtGui import QFont
from PyQt5.QtCore import QCoreApplication


class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.center()

    def initUI(self):

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('File')

        exitAct = QAction('&Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(self.close)

        impMenu = QMenu('Import', self)
        impAct = QAction('Import mail', self)
        impMenu.addAction(impAct)

        newAct = QAction('New', self)

        viewMenu = menubar.addMenu('View')

        viewStatAct = QAction('View statusbar', self, checkable=True)
        viewStatAct.setStatusTip('View statusbar')
        viewStatAct.setChecked(True)
        viewStatAct.triggered.connect(self.toggleMenu)

        openWidget = QAction("&Open Widget", self)
        openWidget.triggered.connect(self.showWidget)

        fileMenu.addAction(newAct)
        fileMenu.addMenu(impMenu)
        fileMenu.addAction(exitAct)
        viewMenu.addAction(viewStatAct)
        viewMenu.addAction(openWidget)

        # add toolbar
        self.toolbar = self.addToolBar('Exit')
        self.toolbar.addAction(exitAct)

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Submenu')

        self.statusBar()

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Simple menu')
        self.statusBar().showMessage('Ready')
        QToolTip.setFont(QFont('SansSerif', 10))

        self.setToolTip('This is a <b>QWidget</b> widget')

        btn = QPushButton('Quit Button', self)
        btn.setToolTip('This is a <b>QPushButton</b> widget<br>Press to Quit')
        btn.clicked.connect(self.close)
        btn.resize(btn.sizeHint())
        btn.move(50, 50)

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Tooltips')
        self.show()

    def showWidget(self):
        self.widget1 = widget()
        self.widget1.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Message',
                                     "Are you sure to quit?", QMessageBox.Yes |
                                     QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def toggleMenu(self, state):
        if state:
            self.statusBar().show()
            self.statusBar().showMessage("Status bar open")
        else:
            self.statusBar().hide()

    def contextMenuEvent(self, event):

        cmenu = QMenu(self)

        newAct = cmenu.addAction("New")
        opnAct = cmenu.addAction("Open")
        quitAct = cmenu.addAction("Quit")
        action = cmenu.exec_(self.mapToGlobal(event.pos()))

        if action == quitAct:
            qApp.quit()


class widget(QWidget):
    def __init__(self):
        super(widget, self).__init__()
        self.initUI()

    def initUI(self):
        m = PlotCanvas()
        title = QLabel('Title')
        author = QLabel('Author')
        review = QLabel('Review')

        titleEdit = QLineEdit()
        authorEdit = QLineEdit()
        reviewEdit = QTextEdit()

        grid = QGridLayout()
        # grid.setSpacing(10)
        #
        grid.addWidget(title, 1, 0)
        # grid.addWidget(titleEdit, 1, 1)
        #
        grid.addWidget(author, 2, 0)
        # grid.addWidget(authorEdit, 2, 1)
        #
        grid.addWidget(review, 3, 0)
        # grid.addWidget(reviewEdit, 3, 1, 5, 1)

        grid.addWidget(m)
        self.setLayout(grid)
        self.setGeometry(300, 300, 350, 300)
        self.setWindowTitle('Review')


class PlotCanvas(FigureCanvas):
    def __init__(self, parent=None):
        self.figure = Figure()

        FigureCanvas.__init__(self, self.figure)

        self.setParent(parent)
        # self.canvas = FigureCanvas(self.figure)
        # self.toolbar = NavigationToolbar2QT(self.canvas, self)
        FigureCanvas.setSizePolicy(self,
                                   QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        # layout = QVBoxLayout()
        # layout.addWidget(self.toolbar)
        # layout.addWidget(self.canvas)
        # self.setLayout(layout)
        self.plot()

    def plot(self):
        self.figure.clear()
        data = [random.random() for i in range(25)]
        ax = self.figure.add_subplot(111)
        ax.plot(data, 'r-')
        ax.set_title('PyQt Matplotlib Example')
        self.draw()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = widget()
    ex.show()
    sys.exit(app.exec_())
