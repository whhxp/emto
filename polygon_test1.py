# ----------------------------------------------
# Script Recorded by ANSYS Electronics Desktop Version 2017.1.0
# 14:20:35  Sep 12, 2017
# ----------------------------------------------

import sys

sys.path.append('E:\Program Files\AnsysEM\AnsysEM18.1\Win64\PythonFiles\DesktopPlugin')
sys.path.append('E:\Program Files\AnsysEM\AnsysEM18.1\Win64')
import ScriptEnv
from point_class import Point
ScriptEnv.Initialize("Ansoft.ElectronicsDesktop")
unit = "mm"
designName = "polygon_test1"
oDesktop.RestoreWindow()
oProject = oDesktop.NewProject()
oProject.InsertDesign("Maxwell 2D", designName, "Transient", "")
oDesign = oProject.SetActiveDesign(designName)


def random_num(radius_range_low, radius_range_high, theta_range_low, theta_range_high):
    radius = random.uniform(radius_range_low, radius_range_high)
    theta = random.uniform(theta_range_low, theta_range_high)
    return radius, theta


def random_area(ox, oy, out_r_Low, out_r_High, out_theta_low, out_theta_high):
    x = 0
    y = 0
    r, theta1 = random_num(radius_range_low=out_r_Low, radius_range_high=out_r_High, theta_range_low=45 / 180 * math.pi,
                           theta_range_high=90 / 180 * math.pi)
    return x, y


def random_polygon(ox, oy, sides, radius_range_low, radius_range_high, theta_range_low, theta_range_high,
                   fixed_point=[], ):
    theta = []
    radius = []
    x = []
    y = []
    r = 0
    theta.clear()
    radius.clear()
    fixed_point_size = len(fixed_point)
    sides = sides - fixed_point_size
    for point in fixed_point:
        x.append(point[0])
        y.append(point[1])

    for i in range(sides):
        r, theta1 = random_num(radius_range_low, radius_range_high, theta_range_low, theta_range_high)
        radius.append(r)
        theta.append(theta1)

    c = 0
    theta_sum = sum(theta)
    for i in range(sides):
        c += theta[i]
        rr = (radius[i])
        x1 = -math.sin(2 * math.pi * c / theta_sum) * rr + ox
        y1 = math.cos(2 * math.pi * c / theta_sum) * rr + oy
        #        x1,y1=pol2cart(radius[i],theta[i])
        x.append(x1)
        y.append(y1)

    x.append(x[0])
    y.append(y[0])
    print('r is {}\ntheta is {}'.format(radius, theta))

    return x, y

def draw_stator():
    global oProject, oDesign
    print('-----Draw Stator finished.')


def draw_rotor():
    global oProject, oDesign
    print('-----Draw rotor finished.')


def project_setup():
    global oProject, oDesign
    print('-----Project setup finished.')


def output_variables():
    global oProject, oDesign
    print('-----output finished.')


def draw_polygon(points, name, flag="", color="(180 0 0)", transparency=0, partCS="Global", UDMID="",
                 Material="vacuum", SurfaceMaterialValue="", SolveInside=True, IsMaterialEditable=True):
    global oProject, oDesign
    oEditor = oDesign.SetActiveEditor("3D Modeler")
    PolylinePoints = []
    PolylineSegments = []
    PolylinePoints.append("NAME:PolylinePoints")
    for point in points:
        PolylinePoints.append([
            "NAME:PLPoint",
            "X:=", str(point.x) + unit,
            "Y:=", str(point.y) + unit,
            "Z:=", str(point.z) + unit])
    PolylineSegments.append("NAME:PolylineSegments")
    for i in range(len(points)):
        PolylineSegments.append([
            "NAME:PLSegment",
            "SegmentType:=", "Line",
            "StartIndex:=", i,
            "NoOfPoints:=", 2
        ])

    lines = [
        "NAME:PolylineParameters",
        "IsPolylineCovered:=", True,
        "IsPolylineClosed:=", True,
        PolylinePoints,
        PolylineSegments,
        [
            "NAME:PolylineXSection",
            "XSectionType:=", "None",
            "XSectionOrient:=", "Auto",
            "XSectionWidth:=", "0mm",
            "XSectionTopWidth:=", "0mm",
            "XSectionHeight:=", "0mm",
            "XSectionNumSegments:=", "0",
            "XSectionBendType:=", "Corner"
        ]
    ]
    attr = [
        "NAME:Attributes",
        "Name:=", name,
        "Flags:=", flag,
        "Color:=", color,
        "Transparency:=", transparency,
        "PartCoordinateSystem:=", partCS,
        "UDMId:=", UDMID,
        "MaterialValue:=", '\"' + Material + '\"',
        "SurfaceMaterialValue:=", '\"' + SurfaceMaterialValue + '\"',
        "SolveInside:=", SolveInside,
        "IsMaterialEditable:=", IsMaterialEditable
    ]
    oEditor.CreatePolyline(lines, attr)
    print('-----draw_polygon finished.')
    # return lines, attr


def assign_material(material):
    global oProject, oDesign
    print('-----assign_material finished.')


points = []
points.append(Point(0, 0, 0))
points.append(Point(0, 5, 0))
points.append(Point(5, 5, 0))
# points.append(Point(5,0 ,0 ))

draw_polygon(points, "square1")

# print lines
# print line2
# print lines==line2
# oEditor.CreatePolyline(line2,attr)
# oEditor.CreatePolyline(line2, attr)
