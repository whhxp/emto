# ----------------------------------------------
# Script Recorded by ANSYS Electronics Desktop Version 2017.1.0
# 17:10:43  9月 24, 2017
# ----------------------------------------------
import ScriptEnv

ScriptEnv.Initialize("Ansoft.ElectronicsDesktop")
oDesktop.RestoreWindow()
oProject = oDesktop.SetActiveProject("Project14")
oDesign = oProject.SetActiveDesign("ipm1")
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.Rotate(
    [
        "NAME:Selections",
        "Selections:=", "rotor",
        "NewPartsModelFlag:=", "Model"
    ],
    [
        "NAME:RotateParameters",
        "RotateAxis:=", "Z",
        "RotateAngle:=", "-22.5deg"
    ])

oEditor.Split(
    [
        "NAME:Selections",
        "Selections:=", "rotor",
        "NewPartsModelFlag:=", "Model"
    ],
    [
        "NAME:SplitToParameters",
        "SplitPlane:=", "ZX",
        "WhichSide:=", "PositiveOnly",
        "SplitCrossingObjectsOnly:=", False,
        "DeleteInvalidObjects:=", True
    ])
oEditor.CreatePolyline(
    [
        "NAME:PolylineParameters",
        "IsPolylineCovered:=", True,
        "IsPolylineClosed:=", True,
        [
            "NAME:PolylinePoints",
            [
                "NAME:PLPoint",
                "X:=", "68mm",
                "Y:=", "24mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "60mm",
                "Y:=", "22mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "60mm",
                "Y:=", "18mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "64mm",
                "Y:=", "16mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "68mm",
                "Y:=", "16mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "70mm",
                "Y:=", "18mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "72mm",
                "Y:=", "18mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "72mm",
                "Y:=", "22mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "70mm",
                "Y:=", "24mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "68mm",
                "Y:=", "24mm",
                "Z:=", "0mm"
            ]
        ],
        [
            "NAME:PolylineSegments",
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 0,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 1,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 2,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 3,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 4,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 5,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 6,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 7,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 8,
                "NoOfPoints:=", 2
            ]
        ],
        [
            "NAME:PolylineXSection",
            "XSectionType:=", "None",
            "XSectionOrient:=", "Auto",
            "XSectionWidth:=", "0mm",
            "XSectionTopWidth:=", "0mm",
            "XSectionHeight:=", "0mm",
            "XSectionNumSegments:=", "0",
            "XSectionBendType:=", "Corner"
        ]
    ],
    [
        "NAME:Attributes",
        "Name:=", "Polyline3",
        "Flags:=", "",
        "Color:=", "(143 175 143)",
        "Transparency:=", 0,
        "PartCoordinateSystem:=", "Global",
        "UDMId:=", "",
        "MaterialValue:=", "\"vacuum\"",
        "SurfaceMaterialValue:=", "\"\"",
        "SolveInside:=", True,
        "IsMaterialEditable:=", True
    ])
oEditor.CreatePolyline(
    [
        "NAME:PolylineParameters",
        "IsPolylineCovered:=", True,
        "IsPolylineClosed:=", True,
        [
            "NAME:PolylinePoints",
            [
                "NAME:PLPoint",
                "X:=", "68mm",
                "Y:=", "14mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "62mm",
                "Y:=", "14mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "62mm",
                "Y:=", "12mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "58mm",
                "Y:=", "10mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "60mm",
                "Y:=", "8mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "62mm",
                "Y:=", "8mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "66mm",
                "Y:=", "10mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "70mm",
                "Y:=", "8mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "72mm",
                "Y:=", "10mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "70mm",
                "Y:=", "14mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "68mm",
                "Y:=", "14mm",
                "Z:=", "0mm"
            ]
        ],
        [
            "NAME:PolylineSegments",
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 0,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 1,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 2,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 3,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 4,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 5,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 6,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 7,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 8,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 9,
                "NoOfPoints:=", 2
            ]
        ],
        [
            "NAME:PolylineXSection",
            "XSectionType:=", "None",
            "XSectionOrient:=", "Auto",
            "XSectionWidth:=", "0mm",
            "XSectionTopWidth:=", "0mm",
            "XSectionHeight:=", "0mm",
            "XSectionNumSegments:=", "0",
            "XSectionBendType:=", "Corner"
        ]
    ],
    [
        "NAME:Attributes",
        "Name:=", "Polyline4",
        "Flags:=", "",
        "Color:=", "(143 175 143)",
        "Transparency:=", 0,
        "PartCoordinateSystem:=", "Global",
        "UDMId:=", "",
        "MaterialValue:=", "\"vacuum\"",
        "SurfaceMaterialValue:=", "\"\"",
        "SolveInside:=", True,
        "IsMaterialEditable:=", True
    ])
oEditor.CreatePolyline(
    [
        "NAME:PolylineParameters",
        "IsPolylineCovered:=", True,
        "IsPolylineClosed:=", True,
        [
            "NAME:PolylinePoints",
            [
                "NAME:PLPoint",
                "X:=", "64mm",
                "Y:=", "6mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "60mm",
                "Y:=", "4mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "62mm",
                "Y:=", "2mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "66mm",
                "Y:=", "2mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "74mm",
                "Y:=", "2mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "72mm",
                "Y:=", "6mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "66mm",
                "Y:=", "6mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "70mm",
                "Y:=", "4mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "68mm",
                "Y:=", "4mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "66mm",
                "Y:=", "4mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "64mm",
                "Y:=", "6mm",
                "Z:=", "0mm"
            ]
        ],
        [
            "NAME:PolylineSegments",
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 0,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 1,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 2,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 3,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 4,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 5,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 6,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 7,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 8,
                "NoOfPoints:=", 2
            ],
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 9,
                "NoOfPoints:=", 2
            ]
        ],
        [
            "NAME:PolylineXSection",
            "XSectionType:=", "None",
            "XSectionOrient:=", "Auto",
            "XSectionWidth:=", "0mm",
            "XSectionTopWidth:=", "0mm",
            "XSectionHeight:=", "0mm",
            "XSectionNumSegments:=", "0",
            "XSectionBendType:=", "Corner"
        ]
    ],
    [
        "NAME:Attributes",
        "Name:=", "Polyline5",
        "Flags:=", "",
        "Color:=", "(143 175 143)",
        "Transparency:=", 0,
        "PartCoordinateSystem:=", "Global",
        "UDMId:=", "",
        "MaterialValue:=", "\"vacuum\"",
        "SurfaceMaterialValue:=", "\"\"",
        "SolveInside:=", True,
        "IsMaterialEditable:=", True
    ])

oEditor.Subtract(
    [
        "NAME:Selections",
        "Blank Parts:=", "rotor",
        "Tool Parts:=", "Polyline3,Polyline4,Polyline5"
    ],
    [
        "NAME:SubtractParameters",
        "KeepOriginals:=", True
    ])

oEditor.DuplicateMirror(
    [
        "NAME:Selections",
        "Selections:=", "rotor,Polyline3,Polyline4,Polyline5",
        "NewPartsModelFlag:=", "Model"
    ],
    [
        "NAME:DuplicateToMirrorParameters",
        "DuplicateMirrorBaseX:=", "0mm",
        "DuplicateMirrorBaseY:=", "0mm",
        "DuplicateMirrorBaseZ:=", "0mm",
        "DuplicateMirrorNormalX:=", "0.707106781186547mm",
        "DuplicateMirrorNormalY:=", "-0.707106781186547mm",
        "DuplicateMirrorNormalZ:=", "0mm"
    ],
    [
        "NAME:Options",
        "DuplicateAssignments:=", False
    ],
    [
        "CreateGroupsForNewObjects:=", False
    ])

oEditor.Rotate(
    [
        "NAME:Selections",
        "Selections:=", "rotor_1,Polyline3_2,Polyline4_2,Polyline5_2",
        "NewPartsModelFlag:=", "Model"
    ],
    [
        "NAME:RotateParameters",
        "RotateAxis:=", "Z",
        "RotateAngle:=", "-45deg"
    ])
oEditor.Connect(
    [
        "NAME:Selections",
        "Selections:=", "rotor_1,rotor"
    ])
