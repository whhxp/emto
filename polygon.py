import math
import random

import matplotlib.patches as mpatches
import numpy as np
from descartes.patch import PolygonPatch
from matplotlib import pyplot as plt
from shapely import affinity
from shapely.geometry import Polygon, Point, LineString, MultiPolygon

from coordinateConv import pol2cart
from figures import set_limits


def plot_ring(xx, yy, label='right', point_name='Point', print_text=False, text_list=[]):
    plt.plot(xx, yy, '--.')
    if print_text:
        xxyy = zip(xx, yy)
        counter = 0
        for x, y in xxyy:
            if text_list == []:
                plt.text(x, y, point_name + ' %d' % counter,
                         ha=label)
            else:
                plt.text(x, y, point_name + ' %d' % text_list[counter],
                         ha=label)
            counter += 1
            # for i, j in zip(xx, yy):
            # plt.annotate('{:.2f},{:.2f}'.format(i, j), xy=(i, j))


def plot_random_num():
    xx = []
    yy = []
    for i in range(500):
        r, theta1 = random_num(radius_range_low=5, radius_range_high=10, theta_range_low=45 / 180 * math.pi,
                               theta_range_high=90 / 180 * math.pi)
        x, y = pol2cart(r, theta1)
        xx.append(x)
        yy.append(y)
    plt.scatter(xx, yy)
    plt.axis('equal')
    plt.show()


def random_num(radius_range_low, radius_range_high, theta_range_low, theta_range_high):
    radius = random.uniform(radius_range_low, radius_range_high)
    theta = random.uniform(theta_range_low, theta_range_high)
    return radius, theta


def random_area(ox, oy, out_r_Low, out_r_High, out_theta_low, out_theta_high):
    x = 0
    y = 0
    r, theta1 = random_num(radius_range_low=out_r_Low, radius_range_high=out_r_High, theta_range_low=45 / 180 * math.pi,
                           theta_range_high=90 / 180 * math.pi)
    return x, y


def random_poly(start_point: Point, end_point: Point, sides, radius_range_low, radius_range_high, theta_range_low,
                theta_range_high, poly: Polygon):
    dir = Point(end_point.x - start_point.x, end_point.y - start_point.y)
    plist = []
    plist.append(start_point)
    theta = []
    radius = []
    x = []
    y = []
    r = 0
    theta.clear()
    radius.clear()

    pm_polygon = []
    # 加入一个随机变量
    if random.random() > 0.5:
        randomness = 1
    else:
        randomness = 0
    # 先由大到小，再由小到大，这样形成一个凸的形状，算是一个约束条件
    for i in range(int(sides)):
        r, theta1 = random_num(radius_range_low, radius_range_high, theta_range_low, theta_range_high)
        radius.append(r)
        theta.append(theta1)

    c = 0
    theta_sum = sum(theta)

    for i in range(sides):
        c += theta[i]
        rr = (radius[i])
        x1 = (-math.sin(math.pi / 2 * c / theta_sum) * rr + start_point.x) * -1
        y1 = (math.cos(math.pi / 2 * c / theta_sum) * rr + start_point.y) * 1
        x.append(x1)
        y.append(y1)
        plist.append(Point(x1, y1))
    x.append(x[0])
    y.append(y[0])
    plist.append(end_point)
    return plist


@DeprecationWarning
def random_polygon(ox, oy, sides, radius_range_low, radius_range_high, theta_range_low, theta_range_high,
                   fixed_point=[], polygon=None, ):
    theta = []
    radius = []
    x = []
    y = []
    r = 0
    theta.clear()
    radius.clear()
    fixed_point_size = len(fixed_point)
    sides = sides - fixed_point_size
    for point in fixed_point:
        x.append(point[0])
        y.append(point[1])

    pm_polygon = []
    # 加入一个随机变量
    if random.random() > 0.5:
        randomness = 1
    else:
        randomness = 0
    # 先由大到小，再由小到大，这样形成一个凸的形状，算是一个约束条件
    for i in range(int(sides / 2)):
        r, theta1 = random_num(radius_range_low, radius_range_high, theta_range_low, theta_range_high)
        length = len(radius)

        if length != 0 and length < ((sides / 4) + randomness):
            while radius[-1] < r:
                r, theta1 = random_num(radius_range_low, radius_range_high, theta_range_low, theta_range_high)
        elif length > ((sides / 4) + randomness):
            while radius[-1] > r:
                r, theta1 = random_num(radius_range_low, radius_range_high, theta_range_low, theta_range_high)
        radius.append(r)
        theta.append(theta1)
    # radius_re = radius[:]
    # radius_re.reverse()
    # radius = radius + radius_re
    radius = radius * 2
    random_pick_pm = random.randint(0, sides / 2 - 1)
    random_2 = random.randint(1, sides / 2 - 2)
    pm_polygon.append(random_pick_pm % sides)
    pm_polygon.append((random_pick_pm + random_2) % sides)
    pm_polygon.append((int)(sides / 2 + random_pick_pm) % sides)
    pm_polygon.append((int)(sides / 2 + random_pick_pm + random_2) % sides)
    pm_polygon.append(random_pick_pm)
    print("pm_polygon", pm_polygon)
    # theta_re = theta[:]
    # theta_re.reverse()
    # theta = theta + theta_re
    theta = theta * 2
    c = 0
    theta_sum = sum(theta)
    for i in range(sides):
        c += theta[i]
        rr = (radius[i])
        x1 = -math.sin(2 * math.pi * c / theta_sum) * rr + ox
        y1 = math.cos(2 * math.pi * c / theta_sum) * rr + oy
        #        x1,y1=pol2cart(radius[i],theta[i])
        x.append(x1)
        y.append(y1)

    x.append(x[0])
    y.append(y[0])
    # print('r is {}\ntheta is {}'.format(radius, theta))

    return x, y, pm_polygon


def plot_wedge(ox, oy, r, width, theta_start, theta_end):
    return mpatches.Wedge((ox, oy), r, theta_start, theta_end, width=width, fill=True, color='0.8', alpha=0.3)
    # wedge2 = mpatches.Wedge((0, 0), 10, 90, 30, width=width, fill=True, color='0.9')
    # ax.add_artist(wedge2)


def rect(r, theta):
    """theta in degrees

    returns tuple; (float, float); (x,y)
    """
    x = r * math.cos(math.radians(theta))
    y = r * math.sin(math.radians(theta))
    return x, y


def get_rect(p1: Point, p2: Point, size: float, direction=None):
    dx = p2.x - p1.x
    dy = p2.y - p1.y
    pm_length = LineString([(p1.x, p1.y), (p2.x, p2.y)]).length
    # PM厚度=永磁面积/永磁长度
    pm_thickness = size / pm_length
    norm = (-dy, dx)

    if dx == 0:
        x3 = p2.x + pm_thickness
        x4 = p1.x + pm_thickness
        y3 = p2.y
        y4 = p1.y
    elif dy == 0:
        x3 = p2.x
        x4 = p1.x
        y3 = p2.y + pm_thickness
        y4 = p1.y + pm_thickness
    else:
        k = dy / dx
        x3 = k * pm_thickness * np.sqrt(1 / (k ** 2 + 1)) + p2.x
        y3 = -pm_thickness * np.sqrt(1 / ((k) ** 2 + 1)) + p2.y
        x4 = k * pm_thickness * np.sqrt(1 / ((k) ** 2 + 1)) + p1.x
        y4 = -pm_thickness * np.sqrt(1 / ((k) ** 2 + 1)) + p1.y
    if direction != None:
        this_dir = [x4 - p1.x, y4 - p1.y]
        # 如果希望得到的方向和实际计算得到的方向相反，重新计算
        if (direction[0] / this_dir[0]) < 0:
            k = dy / dx
            x3 = -k * pm_thickness * np.sqrt(1 / (k ** 2 + 1)) + p2.x
            y3 = pm_thickness * np.sqrt(1 / ((k) ** 2 + 1)) + p2.y
            x4 = -k * pm_thickness * np.sqrt(1 / ((k) ** 2 + 1)) + p1.x
            y4 = pm_thickness * np.sqrt(1 / ((k) ** 2 + 1)) + p1.y
    # print([dx, dy])
    # print(norm)
    pm_point1 = Point(p1.x, p1.y)
    pm_point2 = Point(p2.x, p2.y)
    pm_point3 = Point(x3, y3)
    pm_point4 = Point(x4, y4)
    # print('point 1=', pm_point1)
    # print('point 2=', pm_point2)
    # print('point 3=', pm_point3)
    # print('point 4=', pm_point4)
    plist = []
    plist.append(pm_point1)
    plist.append(pm_point2)
    plist.append(pm_point3)
    plist.append(pm_point4)

    return plist


def get_random_point_in_polygon(poly: Polygon):
    (minx, miny, maxx, maxy) = poly.bounds
    while True:
        p = Point(random.uniform(minx, maxx), random.uniform(miny, maxy))
        if poly.contains(p):
            return p


def veryfy_point(point: Point, poly: Polygon):
    if poly.contains(point):
        return True
    else:
        return False


def ipm_polygon2(sides: int, pm_size: float, xoff=0, yoff=0, theta=0, width_step=0.1, width_length_ratio=20,
                 wedge: Polygon = None,
                 plot=False, ax=plt.gca()):
    pm = Polygon()
    flux_barrier_L = Polygon()
    flux_barrier_R = Polygon()
    aa=Polygon()
    # 第一步，在直角坐标系生成形状
    for i in range(100):
        # print('step1. Generate polygon from (0,0), including one PM and two flux barriers')
        # 生成永磁体厚度，这是一个变量，同时也决定了长度，我这里设置永磁体的体积是受约束的
        width = np.random.randint(np.sqrt(pm_size / width_length_ratio) / width_step,
                                  np.sqrt(pm_size / 2) / width_step) * width_step
        # print('pm width=', width)
        pm_length = np.floor(pm_size / width / width_step) * width_step
        plist = get_rect(Point([(-pm_length / 2, 0)]), Point([(pm_length / 2, 0)]), pm_length * width)
        # print(plist)
        pm = Polygon([p.x, p.y] for p in plist)

        # 左边磁障
        con_left_plist = get_rect(Point([(-pm_length / 2, 0)]), Point([(-pm_length * 0.8, 0)]),
                                  pm_length * width * 0.3)  # 约束区域
        con_left = Polygon([p.x, p.y] for p in con_left_plist)
        # 上面是约束区域，下面开始在约束区域内生成磁障形状
        flux_barrier_L_list = []
        x_L_vec = [con_left_plist[1].x - con_left_plist[0].x, con_left_plist[1].y - con_left_plist[0].y]
        y_L_vec = [con_left_plist[3].x - con_left_plist[0].x, con_left_plist[3].y - con_left_plist[0].y]
        # print('xvector=', x_L_vec, 'yvector=', y_L_vec)
        flux_barrier_L_list.append(Point(0, 0))
        flux_barrier_L_list.append(Point(np.random.rand() * (x_L_vec[0] + y_L_vec[0]), 0))
        prev = np.random.rand()
        flux_barrier_L_list.append(
            Point(np.random.rand() * (x_L_vec[0] + y_L_vec[0]), prev * (x_L_vec[1] + y_L_vec[1])))
        for i in range(sides - 1):
            current = np.random.uniform(prev, 1)
            flux_barrier_L_list.append(
                Point(np.random.rand() * (x_L_vec[0] + y_L_vec[0]),
                      np.random.rand() * x_L_vec[1] + current * y_L_vec[1]))
            prev = current
        flux_barrier_L_list.append(Point(0, y_L_vec[1]))
        flux_barrier_L = affinity.affine_transform(Polygon([p.x, p.y] for p in flux_barrier_L_list),
                                                   [1, 0, 0, 1, -pm_length / 2, 0])

        # 右边磁障
        con_right_plist = get_rect(Point([(pm_length / 2, 0)]), Point([(pm_length * 0.8, 0)]),
                                   pm_length * width * 0.3)  # 约束区域
        con_right = Polygon([p.x, p.y] for p in con_right_plist)

        # 上面是约束区域，下面开始在约束区域内生成磁障形状
        flux_barrier_R_list = []
        x_R_vec = [con_left_plist[0].x - con_left_plist[1].x, con_left_plist[0].y - con_left_plist[1].y]
        y_R_vec = [con_left_plist[3].x - con_left_plist[0].x, con_left_plist[3].y - con_left_plist[0].y]
        # print('xvector=', x_R_vec, 'yvector=', y_R_vec)
        flux_barrier_R_list.append(Point(0, 0))
        flux_barrier_R_list.append(Point(np.random.rand() * (x_R_vec[0] + y_R_vec[0]), 0))
        prev = np.random.rand()
        flux_barrier_R_list.append(
            Point(np.random.rand() * (x_R_vec[0] + y_R_vec[0]), prev * (x_R_vec[1] + y_R_vec[1])))
        for i in range(sides - 1):
            current = np.random.uniform(prev, 1)
            flux_barrier_R_list.append(
                Point(np.random.rand() * (x_R_vec[0] + y_R_vec[0]),
                      np.random.rand() * x_R_vec[1] + current * y_R_vec[1]))
            prev = current
        flux_barrier_R_list.append(Point(0, y_R_vec[1]))
        # 平移
        flux_barrier_R = affinity.affine_transform(Polygon([p.x, p.y] for p in flux_barrier_R_list),
                                                   [1, 0, 0, 1, pm_length / 2, 0])

        # 第二步，平移，旋转到合适的位置
        # print('step2. move and rotate polygons')
        matrix1 = [np.cos(theta), -np.sin(theta), np.sin(theta), np.cos(theta), xoff, yoff]
        pm = affinity.affine_transform(pm, matrix1)
        con_left = affinity.affine_transform(con_left, matrix1)
        con_right = affinity.affine_transform(con_right, matrix1)
        flux_barrier_L = affinity.affine_transform(flux_barrier_L, matrix1)
        flux_barrier_R = affinity.affine_transform(flux_barrier_R, matrix1)

        if wedge.contains(flux_barrier_R) and wedge.contains(flux_barrier_L):
            aa = pm.union(flux_barrier_L).union(flux_barrier_R)
            print(type(aa))
            if type(aa)==type(MultiPolygon()):
                print(type(aa))
                continue
            else:
                break
        else:
            continue

    # 画图
    if plot is True:
        x1, y1 = pm.exterior.xy
        x_L, y_L = con_left.exterior.xy
        x_R, y_R = con_right.exterior.xy
        fb_x_L, fb_y_L = flux_barrier_L.exterior.xy
        fb_x_R, fb_y_R = flux_barrier_R.exterior.xy
        aa_x, aa_y = aa.exterior.xy
        # ax.fill(fb_x_L, fb_y_L, 'k', alpha=0.7)
        # ax.fill(fb_x_R, fb_y_R, 'k', alpha=0.7)

        ax.fill(x_L, y_L, 'c', alpha=0.3)
        ax.fill(x_R, y_R, 'c', alpha=0.3)

        ax.fill(aa_x, aa_y, 'r', alpha=0.3)
        ax.fill(x1, y1, 'm', alpha=0.3)
        ax.arrow((x1[0] + x1[1]) / 2, (y1[0] + y1[1]) / 2, (x1[2] + x1[3]) / 2 - (x1[0] + x1[1]) / 2,
                 (y1[2] + y1[3]) / 2 - (y1[0] + y1[1]) / 2, width=width / 100, head_width=width / 5,
                 head_length=width / 5, fc='k',
                 ec='k')
        print('pm size = ', pm.area)
    if True:
        return pm, aa
    else:
        return pm, flux_barrier_L, flux_barrier_R


@DeprecationWarning
def ipm_polygon(sides: int, radius_range_low, radius_range_high, theta_range_low, theta_range_high, pm_size):
    # 其中两个点和一个宽度构成永磁体
    # 然后剩余的点构成磁障
    radius = []
    theta = []
    x = []
    y = []
    for i in range(2):
        r, theta1 = random_num(radius_range_low, radius_range_high, theta_range_low, theta_range_high)
        radius.append(r)
        theta.append(theta1)
        x1, y1 = rect(r, theta1 / math.pi * 180)
        x.append(x1)
        y.append(y1)
    # x=[0,1]
    # y=[1,0]
    # 求法线，相当于向量旋转90度，详见https://en.wikipedia.org/wiki/Rotation_matrix#Basic_rotations
    plist = get_rect(Point([(x[0], y[0])]), Point([(x[1], y[1])]), pm_size)
    pm = Polygon([p.x, p.y] for p in plist)
    x1, y1 = pm.exterior.xy
    print(pm.area)

    # PM左边的磁障生成
    # 这里的方向是希望得到的方向，即不和PM重叠
    constraint_area1_list = get_rect(plist[1], plist[2], size=0.5,
                                     direction=[plist[1].x - plist[0].x, plist[1].y - plist[0].y])

    constraint_area1 = Polygon([p.x, p.y] for p in constraint_area1_list)
    constraint_area1_x, constraint_area1_y = constraint_area1.exterior.xy
    constraint_area_length = LineString([constraint_area1_list[1], constraint_area1_list[2]]).length
    flux_barrier1_list = random_poly(constraint_area1_list[0], constraint_area1_list[1], sides=sides,
                                     radius_range_low=0, radius_range_high=constraint_area_length, theta_range_low=0,
                                     theta_range_high=math.pi / 2, poly=constraint_area1)

    # PM右边的磁障生成
    # 这里的方向是希望得到的方向，即不和PM重叠
    constraint_area2_list = get_rect(plist[0], plist[3], size=0.5,
                                     direction=[plist[0].x - plist[1].x, plist[0].y - plist[1].y])
    constraint_area2 = Polygon([p.x, p.y] for p in constraint_area2_list)

    constraint_area2_x, constraint_area2_y = constraint_area2.exterior.xy
    flux_barrier2_list = random_poly(constraint_area2_list[0], constraint_area2_list[1], sides=sides,
                                     radius_range_low=0, radius_range_high=constraint_area_length, theta_range_low=0,
                                     theta_range_high=math.pi / 2, poly=constraint_area2)

    # 画图
    flux_barrier1 = Polygon([p.x, p.y] for p in flux_barrier1_list)
    flux_barrier2 = Polygon([p.x, p.y] for p in flux_barrier2_list)
    flux_barrier1_x, flux_barrier1_y = flux_barrier1.exterior.xy
    flux_barrier2_x, flux_barrier2_y = flux_barrier2.exterior.xy
    plt.axis('equal')
    k1 = (plist[1].y - plist[0].y) / (plist[1].x - plist[0].x)
    k2 = (plist[2].y - plist[1].y) / (plist[2].x - plist[1].x)
    k3 = (plist[3].y - plist[0].y) / (plist[3].x - plist[0].x)
    plt.fill(x1, y1, 'b', alpha=0.3)
    plt.fill(flux_barrier1_x, flux_barrier1_y, 'r')
    plt.fill(flux_barrier2_x, flux_barrier2_y, 'r')
    plt.fill(constraint_area1_x, constraint_area1_y, 'y', alpha=0.3)
    plt.fill(constraint_area2_x, constraint_area2_y, 'y', alpha=0.3)

    plt.text((x1[2] + x1[0]) / 2, (y1[2] + y1[0]) / 2, "PM k=%f" % k1)
    plt.text((constraint_area1_x[0] + constraint_area1_x[2]) / 2, (constraint_area1_y[0] + constraint_area1_y[2]) / 2,
             "Flux Barrier1 k=%f" % k2)
    plt.text((constraint_area2_x[0] + constraint_area2_x[2]) / 2, (constraint_area2_y[0] + constraint_area2_y[2]) / 2,
             "Flux Barrier2 k=%f" % k3)


def test_pm():
    ipm_polygon(sides=10, radius_range_low=5, radius_range_high=10,
                theta_range_low=0 / 180 * math.pi,
                theta_range_high=30 / 180 * math.pi, pm_size=0.5)


def test_random_polygon():
    for i in range(10):
        sides = 22
        # central point
        ox, oy = pol2cart(7, 30 / 180 * math.pi)
        ax = plt.figure(1)
        ax.add_artist(plot_wedge(0, 0, 10, 5, 0, 45))
        plt.axis('equal')
        plt.axis([3, 10, 0, 8])
        x = []
        y = []
        pm = []
    while True:
        x, y, pm = random_polygon(ox=ox, oy=oy, sides=sides, radius_range_low=0.5, radius_range_high=1,
                                  theta_range_low=0 / 180 * math.pi,
                                  theta_range_high=90 / 180 * math.pi, fixed_point=[])

        pm_list_x = []
        pm_list_y = []
        pm_list = []
        for i in pm:
            pm_list_x.append(x[i])
            pm_list_y.append(y[i])
            pm_list.append([x[i], y[i]])

        pm1 = Polygon(pm_list)
        # print(pm1.area)
        if (pm1.area > 1.5):
            break

    plot_ring(x, y)
    plot_ring(pm_list_x, pm_list_y, label='left', point_name='PM Point', print_text=True, text_list=pm)
    ax.plot(ox, oy, '.')
    plt.show()


def simplify():
    from matplotlib import pyplot
    from shapely.geometry import Point
    from descartes.patch import PolygonPatch

    from figures import SIZE, BLUE, GRAY, set_limits

    fig = pyplot.figure(1, figsize=SIZE, dpi=90)  # 1, figsize=SIZE, dpi=90)

    p = Point(1, 1).buffer(1.5)

    # 1
    ax = fig.add_subplot(121)

    q = p.simplify(0.1)

    patch1a = PolygonPatch(p, facecolor=GRAY, edgecolor=GRAY, alpha=0.5, zorder=1)
    ax.add_patch(patch1a)

    patch1b = PolygonPatch(q, facecolor=BLUE, edgecolor=BLUE, alpha=0.5, zorder=2)
    ax.add_patch(patch1b)

    ax.set_title('a) tolerance 0.1')

    set_limits(ax, -1, 3, -1, 3)

    # 2
    ax = fig.add_subplot(122)

    r = p.simplify(0.5)

    patch2a = PolygonPatch(p, facecolor=GRAY, edgecolor=GRAY, alpha=0.5, zorder=1)
    ax.add_patch(patch2a)

    patch2b = PolygonPatch(r, facecolor=BLUE, edgecolor=BLUE, alpha=0.5, zorder=2)
    ax.add_patch(patch2b)

    ax.set_title('b) tolerance 0.5')

    set_limits(ax, -1, 3, -1, 3)

    pyplot.show()


# 生成磁障
def test_flux_barrier(wedge=None, plot=False, ax=plt.gca()):
    print('polygon generation')
    filepath = 'list.py'
    union = True
    # ax.add_artist(plot_wedge(ox=0, oy=0, r=81, width=40, theta_start=0, theta_end=45))
    for i in range(1):
        if union is not True:
            pm, FB_right, FB_left = ipm_polygon2(sides=20, pm_size=20, plot=plot, xoff=np.random.uniform(50, 70),
                                                 yoff=np.random.uniform(5, 20),
                                                 theta=np.random.uniform(-np.pi, np.pi), ax=ax, wedge=wedge)
            pm_list = list(pm.exterior.coords)
            fb_right_list = list(FB_right.exterior.coords)
            fb_left_list = list(FB_left.exterior.coords)
            print(pm_list)
            print(fb_right_list)
            print(fb_left_list)

            lines = []
            with open(filepath, 'r') as f:
                lines = f.readlines()
            with open(filepath, 'w') as file_handler:
                file_handler.write('pm_list=[')
                for item in pm_list:
                    file_handler.write("{}".format(item))
                    if item is not pm_list[-1]:
                        file_handler.write(',')
                file_handler.write(']\n')
                file_handler.write('fb_right_list=[')
                for item in fb_right_list:
                    file_handler.write("{}".format(item))
                    if item is not pm_list[-1]:
                        file_handler.write(',')
                file_handler.write(']\n')
                file_handler.write('fb_left_list=[')
                for item in fb_left_list:
                    file_handler.write("{}".format(item))
                    if item is not pm_list[-1]:
                        file_handler.write(',')
                file_handler.write(']\n')
                file_handler.writelines(lines)
        else:
            pm, aa = ipm_polygon2(sides=20, pm_size=20, plot=plot, xoff=np.random.uniform(50, 70),
                                  yoff=np.random.uniform(5, 20),
                                  theta=np.random.uniform(-np.pi, np.pi), ax=ax, wedge=wedge)
            pm_list = list(pm.exterior.coords)
            aa_list = list(aa.exterior.coords)
            print(pm_list)
            print(aa_list)

            lines = []
            with open(filepath, 'r') as f:
                lines = f.readlines()
            with open(filepath, 'w') as file_handler:
                file_handler.write('pm_list=[')
                for item in pm_list:
                    file_handler.write("{}".format(item))
                    if item is not pm_list[-1]:
                        file_handler.write(',')
                file_handler.write(']\n')
                file_handler.write('aalist=[')
                for item in aa_list:
                    file_handler.write("{}".format(item))
                    if item is not pm_list[-1]:
                        file_handler.write(',')
                file_handler.write(']\n')
                file_handler.writelines(lines)

def wedge_shapely(rout=81, rin=40, theta_min=0 / 180 * np.pi, theta_max=(22.5 / 180 * np.pi), plot: bool = False,
                  ax=plt.gca()):
    # variables for buffers
    coordinates = (0, 0)
    circle1 = Point(coordinates).buffer(rin)
    circle2 = Point(coordinates).buffer(rout)
    if theta_max < theta_min or theta_max > 90 / 180 * np.pi or theta_min < 0:
        print('Theta Max < Theta Min, Error')
    elif theta_min < 45 / 180 * np.pi and theta_max > 45 / 180 * np.pi:
        x1 = 0
        y1 = 0
        x2 = rout
        y2 = rout * np.tan(theta_min)
        x3 = rout
        y3 = rout
        x4 = (rout / np.tan(theta_max))
        y4 = rout
        polygon = Polygon([(x1, y1), (x2, y2), (x3, y3), (x4, y4)])
    elif theta_min > 45 / 180 * np.pi:

        x1 = 0
        y1 = 0
        x2 = rout / np.tan(theta_min)
        y2 = rout
        x3 = rout / np.tan(theta_max)
        y3 = rout
        polygon = Polygon([(x1, y1), (x2, y2), (x3, y3)])
    elif theta_max <= 45 / 180 * np.pi:
        x1 = 0
        y1 = 0
        x2 = rout
        y2 = rout * np.tan(theta_min)
        x3 = rout
        y3 = rout * np.tan(theta_max)
        polygon = Polygon([(x1, y1), (x2, y2), (x3, y3)])

    ring = circle2.difference(circle1)
    wedge = ring.intersection(polygon)

    if plot:
        patch1a = PolygonPatch(wedge, alpha=0.3)
        ax.add_patch(patch1a)

    return wedge


def verify(design_domain_list, shape_list):
    print('verify polygon inside')
    design_domain = Polygon()



if __name__ == '__main__':
    fig = plt.figure(1)  # 1, figsize=SIZE, dpi=90)
    ax = fig.add_subplot(111)
    plot = True
    wedge = wedge_shapely(plot=plot, ax=ax)
    print(list(wedge.exterior.coords))
    test_flux_barrier(wedge=wedge, ax=ax, plot=plot)

    if plot:
        plt.axis('equal')
        set_limits(ax, -10, int(100), -20, int(50))
        plt.show()
