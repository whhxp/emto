# -*- coding: utf-8 -*-

x = 50


def func():
    global x
    print(u'x的值是', x)
    x = 2
    print(u'全局变量x改为', x)


func()
print(u'x的值是', x)
