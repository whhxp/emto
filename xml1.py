import clr

clr.AddReference('System.Xml.Linq')
from System.Xml.Linq import *

contacts = XElement(XName.Get("Contracts"), XElement(XName.Get("Contact")
                                                     , XElement(XName.Get("Name"), "Patrick Hines")
                                                     , XElement(XName.Get("Phone"), "206-555-0144")
                                                     , XElement(XName.Get("Address")
                                                                , XElement(XName.Get("Street1"), "123 Main St")
                                                                , XElement(XName.Get("City"), "Mercer Island")
                                                                , XElement(XName.Get("State"), "WA")
                                                                , XElement(XName.Get("Postal"), "68042")
                                                                )
                                                     )
                    )

polygons = XElement(XName.Get('polygons')
                    , XElement(XName.Get('barrier1')
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 68),
                                          XElement(XName.Get('y'), 24),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 60),
                                          XElement(XName.Get('y'), 22),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 60),
                                          XElement(XName.Get('y'), 18),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 64),
                                          XElement(XName.Get('y'), 16),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 68),
                                          XElement(XName.Get('y'), 16),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 70),
                                          XElement(XName.Get('y'), 18),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 72),
                                          XElement(XName.Get('y'), 18),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 72),
                                          XElement(XName.Get('y'), 22),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 70),
                                          XElement(XName.Get('y'), 24),
                                          )
                               )
                    , XElement(XName.Get('magnet')
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 68),
                                          XElement(XName.Get('y'), 14),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 62),
                                          XElement(XName.Get('y'), 14),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 62),
                                          XElement(XName.Get('y'), 12),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 58),
                                          XElement(XName.Get('y'), 10),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 60),
                                          XElement(XName.Get('y'), 8),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 62),
                                          XElement(XName.Get('y'), 8),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 66),
                                          XElement(XName.Get('y'), 10),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 70),
                                          XElement(XName.Get('y'), 8),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 72),
                                          XElement(XName.Get('y'), 10),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 70),
                                          XElement(XName.Get('y'), 14),
                                          )

                               )
                    , XElement(XName.Get('barrier2')
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 64),
                                          XElement(XName.Get('y'), 6),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 60),
                                          XElement(XName.Get('y'), 4),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 62),
                                          XElement(XName.Get('y'), 2),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 66),
                                          XElement(XName.Get('y'), 2),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 74),
                                          XElement(XName.Get('y'), 2),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 72),
                                          XElement(XName.Get('y'), 6),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 66),
                                          XElement(XName.Get('y'), 6),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 70),
                                          XElement(XName.Get('y'), 4),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 68),
                                          XElement(XName.Get('y'), 4),
                                          )
                               , XElement(XName.Get('Points'),
                                          XElement(XName.Get('x'), 66),
                                          XElement(XName.Get('y'), 4),
                                          )
                               )

                    )

contacts.Save('a.xml')

# print contacts
lala = XDocument().Load('a.xml')
for item in lala.Element('Contracts') \
        .Element('Contact') \
        .Element('Address').Elements():
    print item
    print '------'

# print 'lala is '
# print lala
import clr

clr.AddReference('System.Xml.Linq')
from System.Xml.Linq import *
from point_class import Point

polygons.Save('polygonList.xml')
p_magnet_list = []
p_barrier1_list = []
p_barrier2_list = []
poly = XDocument().Load('polygonList.xml')
for points in poly.Element('polygons').Element('magnet').Elements():
    p = Point()
    p.x = points.Element('x').Value
    p.y = points.Element('y').Value
    p_magnet_list.append(p)

for points in poly.Element('polygons').Element('barrier1').Elements():
    p = Point()
    p.x = points.Element('x').Value
    p.y = points.Element('y').Value
    p_barrier1_list.append(p)

for points in poly.Element('polygons').Element('barrier2').Elements():
    p = Point()
    p.x = points.Element('x').Value
    p.y = points.Element('y').Value
    p_barrier2_list.append(p)

print p_magnet_list
print p_barrier1_list
print p_barrier2_list
