import math


# # evenly sampled time at 200ms intervals
# t = np.arange(0., 5., 0.2)
#
# # red dashes, blue squares and green triangles
# plt.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^')
# plt.show()


def cart2pol(x, y):
    rho = math.sqrt(x ** 2 + y ** 2)
    phi = math.atan2(y, x)
    return (rho, phi)


def pol2cart(rho, phi):
    x = rho * math.cos(phi)
    y = rho * math.sin(phi)
    return (x, y)


if __name__ == "__main__":
    x1 = 5
    y1 = 5
    x2 = 6
    y2 = 6
    print(cart2pol(x2 - x1, y2 - y1))
