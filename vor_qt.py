import sys

import numpy as np
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import (QMainWindow, QWidget, QAction, qApp, QToolTip,
                             QDesktopWidget,
                             QPushButton, QApplication, QMessageBox, QMenu,
                             QLabel, QLineEdit,
                             QGridLayout, QSizePolicy
                             )
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT
from matplotlib.figure import Figure
from scipy.spatial import Voronoi
from atlas import Atlas
from Voronoi_color import color_plt
from polygon import plot_wedge
from coordinateConv import pol2cart
import random
from array import array
import math
from scipy.spatial import Delaunay


class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.center()

    def initUI(self):

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('File')

        exitAct = QAction('&Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(self.close)

        impMenu = QMenu('Import', self)
        impAct = QAction('Import mail', self)
        impMenu.addAction(impAct)

        newAct = QAction('New', self)

        viewMenu = menubar.addMenu('View')

        viewStatAct = QAction('View statusbar', self, checkable=True)
        viewStatAct.setStatusTip('View statusbar')
        viewStatAct.setChecked(True)
        viewStatAct.triggered.connect(self.toggleMenu)

        openWidget = QAction("&Open Widget", self)
        openWidget.triggered.connect(self.showWidget)

        fileMenu.addAction(newAct)
        fileMenu.addMenu(impMenu)
        fileMenu.addAction(exitAct)
        viewMenu.addAction(viewStatAct)
        viewMenu.addAction(openWidget)

        # add toolbar
        self.toolbar = self.addToolBar('Exit')
        self.toolbar.addAction(exitAct)
        self.toolbar.addAction(openWidget)

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Submenu')

        self.statusBar()

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Simple menu')
        self.statusBar().showMessage('Ready')
        QToolTip.setFont(QFont('SansSerif', 10))

        self.setToolTip('This is a <b>QWidget</b> widget')

        btn = QPushButton('Quit Button', self)
        btn.setToolTip('This is a <b>QPushButton</b> widget<br>Press to Quit')
        btn.clicked.connect(self.close)
        btn.resize(btn.sizeHint())
        btn.move(50, 50)

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Tooltips')
        self.show()

    def showWidget(self):
        self.widget1 = widget()
        self.widget1.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Message',
                                     "Are you sure to quit?", QMessageBox.Yes |
                                     QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def toggleMenu(self, state):
        if state:
            self.statusBar().show()
            self.statusBar().showMessage("Status bar open")
        else:
            self.statusBar().hide()

    def contextMenuEvent(self, event):

        cmenu = QMenu(self)

        newAct = cmenu.addAction("New")
        opnAct = cmenu.addAction("Open")
        quitAct = cmenu.addAction("Quit")
        action = cmenu.exec_(self.mapToGlobal(event.pos()))

        if action == quitAct:
            qApp.quit()


class widget(QWidget):
    def __init__(self):
        super(widget, self).__init__()
        self.initUI()
        self.show()

    def get_num(self, num_edit):
        try:
            num = int(num_edit.text())
            if num < 3:
                num = 3
                print("num should larger than 3, now num = 3")
        except ValueError:
            num = 3
            print('num error, num = 3')
        self.m.plotVor(num)

    def initUI(self):
        self.m = PlotCanvas()
        title = QLabel('Title')
        Number = QLabel('Number')
        review = QLabel('Review')
        plotBtn = QPushButton('Plot Random Voronoi Diagram')
        titleEdit = QLineEdit()
        num_edit = QLineEdit()
        titleEdit.setText('Plot')

        plotBtn.clicked.connect(lambda: self.get_num(num_edit))

        navi = NavigationToolbar2QT(self.m.figure.canvas, parent=self.parent())
        grid = QGridLayout()
        grid.setSpacing(10)
        #
        grid.addWidget(title, 1, 0)
        grid.addWidget(titleEdit, 1, 1)
        #
        grid.addWidget(Number, 2, 0)
        grid.addWidget(num_edit, 2, 1)
        grid.addWidget(plotBtn, 3, 0, 1, 1)
        grid.addWidget(navi, 4, 0, 2, 2)
        grid.addWidget(self.m, 7, 0, -1, -1)

        self.setLayout(grid)
        self.setGeometry(300, 300, 350, 300)
        self.setWindowTitle('Review')

    def contextMenuEvent(self, event):

        cmenu = QMenu(self)

        newAct = cmenu.addAction("New")
        opnAct = cmenu.addAction("Open")
        quitAct = cmenu.addAction("Quit")
        action = cmenu.exec_(self.mapToGlobal(event.pos()))

        if action == quitAct:
            self.close()

        if action == opnAct:
            print('open action')


class PlotCanvas(FigureCanvas):
    def __init__(self, parent=None):
        self.figure = Figure()

        FigureCanvas.__init__(self, self.figure)

        self.setParent(parent)
        FigureCanvas.setSizePolicy(self,
                                   QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def random_num(self, radius_range_low, radius_range_high, theta_range_low, theta_range_high):
        radius = random.uniform(radius_range_low, radius_range_high)
        theta = random.uniform(theta_range_low, theta_range_high)
        return radius, theta

    def wedge_points(self, num):
        xx = []
        yy = []
        points = []
        for i in range(num):
            r, theta1 = self.random_num(radius_range_low=5, radius_range_high=10, theta_range_low=45 / 180 * math.pi,
                                        theta_range_high=90 / 180 * math.pi)
            x, y = pol2cart(r, theta1)
            xx.append(x)
            yy.append(y)
        return xx, yy

    def plotVor(self, num):
        points = np.random.rand(num, 2)
        xx, yy = self.wedge_points(num)
        # this operation I get form https://stackoverflow.com/questions/18730044/converting-two-lists-into-a-matrix
        wpoint = np.column_stack((xx, yy))
        atlas1 = Atlas(dimensions=(15, 15), granularity=100)
        atlas2 = Atlas(wpoint)
        vor = atlas1.generate_voronoi()
        print("filtered region before relax")
        print(atlas1.filtered_regions)
        vor2 = atlas1.relax_points(2)
        print("filtered region after relax")
        print(atlas1.filtered_regions)
        print(len(vor.points))
        print(len(vor2.points))
        # vor2=vor
        self.figure.clear()
        self.figure.suptitle('Title', fontsize=26, fontweight=50)
        ax = self.figure.add_subplot(331)
        ax2 = self.figure.add_subplot(332)
        axRelax = self.figure.add_subplot(333)
        ax3 = self.figure.add_subplot(334)
        ax4 = self.figure.add_subplot(335)
        ax5 = self.figure.add_subplot(336)
        ax6 = self.figure.add_subplot(337)
        ax.set_title('plot random voronoi diagram')
        # voronoi_plot_2d(vor, ax=ax)
        color_plt(points=vor.points, vor=vor, ax=ax)
        color_plt(points=vor2.points, vor=vor2, ax=axRelax)
        ax2.set_title('scatter of points')
        ax2.scatter(points[:, 0], points[:, 1])
        ax3.set_title('x range')
        ax3.hist(points[:, 0])
        ax4.set_title('y range')
        ax4.hist(points[:, 1])
        ax5.set_title('box plot')
        ax5.boxplot(points)

        wedge = plot_wedge(0, 0, 10, 5, 45, 90)
        # ax6.add_artist(wedge)
        vor3 = atlas2.generate_voronoi()
        print(atlas2.filtered_regions)
        # vor3 = atlas2.relax_points(2)
        color_plt(vor=vor3, points=vor3.points, ax=ax6)
        # print(wpoint)
        # print(points)
        ax6.axis('equal')
        ax6.axis([0, 15, 0, 15])

        tri = Delaunay(wpoint)
        ax7 = self.figure.add_subplot(338)
        ax7.triplot(wpoint[:, 0], wpoint[:, 1], tri.simplices)
        ax7.plot(wpoint[:, 0], wpoint[:, 1], 'o')
        # for j, p in enumerate(wpoint):
        #     ax7.text(p[0] - 0.03, p[1] + 0.03, j, ha='right')  # label the points
        # for j, s in enumerate(tri.simplices):
        #     p = points[s].mean(axis=0)
        #     ax7.text(p[0], p[1], '#%d' % j, ha='center')  # label triangles
        ax7.axis([0, 15, 0, 15])
        ax6.axis('equal')
        self.draw()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainwindow = App()
    sys.exit(app.exec_())
