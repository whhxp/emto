# ----------------------------------------------
# Script Recorded by ANSYS Electronics Desktop Version 2017.1.0
# 15:33:10  9月 24, 2017
# ----------------------------------------------
import ScriptEnv

ScriptEnv.Initialize("Ansoft.ElectronicsDesktop")
oDesktop.RestoreWindow()
oProject = oDesktop.SetActiveProject("Project16")
oDesign = oProject.SetActiveDesign("ipm1")
oModule = oDesign.GetModule("BoundarySetup")
oModule.AssignMaster(
    [
        "NAME:Master1",
        "Edges:=", [41634],
        "ReverseV:=", False
    ])
oModule.AssignSlave(
    [
        "NAME:Slave1",
        "Edges:=", [41645],
        "ReverseU:=", True,
        "Master:=", "Master1",
        "SameAsMaster:=", False
    ])
oModule.AssignVectorPotential(
    [
        "NAME:zeroflux",
        "Edges:=", [41644, 41643, 41642, 41641, 41640, 41639],
        "Value:=", "0",
        "CoordinateSystem:=", ""
    ])
oProject.Save()
oDesign.AnalyzeAll()
oModule = oDesign.GetModule("FieldsReporter")
oModule.CreateFieldPlot(
    [
        "NAME:Mag_B1",
        "SolutionName:=", "Setup1 : Transient",
        "QuantityName:=", "Mag_B",
        "PlotFolder:=", "B",
        "UserSpecifyName:=", 0,
        "UserSpecifyFolder:=", 0,
        "StreamlinePlot:=", False,
        "IntrinsicVar:=", "Time=\'0.015s\'",
        "PlotGeomInfo:=",
        [1, "Surface", "FacesList", 13, "6", "6879", "30323", "30725", "33540", "36355", "39170", "39572", "40267",
         "40674", "40851", "41633", "41657"],
        "FilterBoxes:=", [0],
        [
            "NAME:PlotOnSurfaceSettings",
            "Filled:=", False,
            "IsoValType:=", "Fringe",
            "SmoothShade:=", True,
            "AddGrid:=", False,
            "MapTransparency:=", True,
            "Refinement:=", 0,
            "Transparency:=", 0,
            [
                "NAME:Arrow3DSpacingSettings",
                "ArrowUniform:=", True,
                "ArrowSpacing:=", 0,
                "MinArrowSpacing:=", 0,
                "MaxArrowSpacing:=", 0
            ],
            "GridColor:=", [255, 255, 255]
        ]
    ])
oModule.SetPlotFolderSettings("B",
                              [
                                  "NAME:FieldsPlotSettings",
                                  "Real time mode:=", True,
                                  [
                                      "NAME:ColorMapSettings",
                                      "ColorMapType:=", "Spectrum",
                                      "SpectrumType:=", "Rainbow",
                                      "UniformColor:=", [127, 255, 255],
                                      "RampColor:=", [255, 127, 127]
                                  ],
                                  [
                                      "NAME:Scale3DSettings",
                                      "unit:=", 103,
                                      "m_nLevels:=", 15,
                                      "minvalue:=", 3.2424E-005,
                                      "maxvalue:=", 2.3368,
                                      "log:=", False,
                                      "IntrinsicMin:=", 3.24239081237465E-005,
                                      "IntrinsicMax:=", 2.33683633804321,
                                      "LimitFieldValuePrecision:=", False,
                                      "FieldValuePrecisionDigits:=", 4,
                                      "dB:=", False,
                                      "ScaleType:=", 1,
                                      "UserSpecifyValues:=",
                                      [16, 3.24239990732167E-005, 0.155816942453384, 0.311601459980011,
                                       0.467385977506638, 0.623170495033264, 0.778954982757568, 0.934739530086517,
                                       1.09052407741547, 1.24630856513977, 1.40209305286407, 1.55787754058838,
                                       1.71366214752197, 1.86944663524628, 2.02523112297058, 2.18101572990417,
                                       2.33680009841919],
                                      "ValueNumberFormatTypeAuto:=", 1,
                                      "ValueNumberFormatTypeScientific:=", True,
                                      "ValueNumberFormatWidth:=", 12,
                                      "ValueNumberFormatPrecision:=", 4
                                  ],
                                  [
                                      "NAME:Marker3DSettings",
                                      "MarkerType:=", 9,
                                      "MarkerMapSize:=", True,
                                      "MarkerMapColor:=", False,
                                      "MarkerSize:=", 0.25
                                  ],
                                  [
                                      "NAME:Arrow3DSettings",
                                      "ArrowType:=", 1,
                                      "ArrowMapSize:=", True,
                                      "ArrowMapColor:=", True,
                                      "ShowArrowTail:=", True,
                                      "ArrowSize:=", 0.25,
                                      "ArrowMinMagnitude:=", -0.499967576091876,
                                      "ArrowMaxMagnitude:=", 2.83683633804321,
                                      "ArrowMagnitudeThreshold:=", 0,
                                      "ArrowMagnitudeFilteringFlag:=", False,
                                      "ArrowMinIntrinsicMagnitude:=", 0,
                                      "ArrowMaxIntrinsicMagnitude:=", 1
                                  ],
                                  [
                                      "NAME:DeformScaleSettings",
                                      "ShowDeformation:=", True,
                                      "MinScaleFactor:=", 0,
                                      "MaxScaleFactor:=", 1,
                                      "DeformationScale:=", 0
                                  ]
                              ])
oModule.SetPlotFolderSettings("B",
                              [
                                  "NAME:FieldsPlotSettings",
                                  "Real time mode:=", True,
                                  [
                                      "NAME:ColorMapSettings",
                                      "ColorMapType:=", "Spectrum",
                                      "SpectrumType:=", "Rainbow",
                                      "UniformColor:=", [127, 255, 255],
                                      "RampColor:=", [255, 127, 127]
                                  ],
                                  [
                                      "NAME:Scale3DSettings",
                                      "unit:=", 103,
                                      "m_nLevels:=", 15,
                                      "minvalue:=", 3.2424E-005,
                                      "maxvalue:=", 1.2,
                                      "log:=", False,
                                      "IntrinsicMin:=", 3.24239081237465E-005,
                                      "IntrinsicMax:=", 2.33683633804321,
                                      "LimitFieldValuePrecision:=", False,
                                      "FieldValuePrecioProject = oDesktop.NewProject()
                                      oProject.InsertDesign("Maxwell 2D", "Maxwell2DDesign1", "Magnetostatic", "")
                                      oDesign = oProject.SetActiveDesign("Maxwell2DDesign1")
oDesign.RenameDesignInstance("Maxwell2DDesign1", "ipm1")
oDesign.SetSolutionType("Transient", "XY")
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.CreateUserDefinedPart(
    [
        "NAME:UserDefinedPrimitiveParameters",
        "DllName:=", "RMxprt/SlotCore.dll",
        "Version:=", "12.1",
        "NoOfParameters:=", 19,
        "Library:=", "syslib",
        [
            "NAME:ParamVector",
            [
                "NAME:Pair",
                "Name:=", "DiaGap",
                "Value:=", "161.90000000000001mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "DiaYoke",
                "Value:=", "269.24000000000001mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Length",
                "Value:=", "80mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Skew",
                "Value:=", "0deg"
            ],
            [
                "NAME:Pair",
                "Name:=", "Slots",
                "Value:=", "18"
            ],
            [
                "NAME:Pair",
                "Name:=", "SlotType",
                "Value:=", "1"
            ],
            [
                "NAME:Pair",
                "Name:=", "Hs0",
                "Value:=", "5mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Hs01",
                "Value:=", "8mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Hs1",
                "Value:=", "5mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Hs2",
                "Value:=", "8mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Bs0",
                "Value:=", "2.5mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Bs1",
                "Value:=", "8mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Bs2",
                "Value:=", "5mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Rs",
                "Value:=", "0mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "FilletType",
                "Value:=", "0"
            ],
            [
                "NAME:Pair",
                "Name:=", "HalfSlot",
                "Value:=", "0"
            ],
            [
                "NAME:Pair",
                "Name:=", "SegAngle",
                "Value:=", "14.999999999999998deg"
            ],
            [
                "NAME:Pair",
                "Name:=", "LenRegion",
                "Value:=", "200mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "InfoCore",
                "Value:=", "0"
            ]
        ]
    ],
    [
        "NAME:Attributes",
        "Name:=", "SlotCore1",
        "Flags:=", "",
        "Color:=", "(128 128 255)",
        "Transparency:=", 0,
        "PartCoordinateSystem:=", "Global",
        "UDMId:=", "",
        "MaterialValue:=", "\"vacuum\"",
        "SurfaceMaterialValue:=", "\"\"",
        "SolveInside:=", True,
        "IsMaterialEditable:=", True
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DCmdTab",
            [
                "NAME:PropServers",
                "SlotCore1:CreateUserDefinedPart:1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Hs2",
                    "Value:=", "28mm"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DCmdTab",
            [
                "NAME:PropServers",
                "SlotCore1:CreateUserDefinedPart:1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Bs2",
                    "Value:=", "0mm"
                ],
                [
                    "NAME:Bs1",
                    "Value:=", "5mm"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DCmdTab",
            [
                "NAME:PropServers",
                "SlotCore1:CreateUserDefinedPart:1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Hs1",
                    "Value:=", "8mm"
                ],
                [
                    "NAME:Hs0",
                    "Value:=", "2.5mm"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DCmdTab",
            [
                "NAME:PropServers",
                "SlotCore1:CreateUserDefinedPart:1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Slots",
                    "Value:=", "48"
                ],
                [
                    "NAME:SlotType",
                    "Value:=", "2"
                ],
                [
                    "NAME:Hs0",
                    "Value:=", "1.03mm"
                ],
                [
                    "NAME:Hs01",
                    "Value:=", "0mm"
                ],
                [
                    "NAME:Hs1",
                    "Value:=", "0mm"
                ],
                [
                    "NAME:Hs2",
                    "Value:=", "29.5mm"
                ],
                [
                    "NAME:Bs0",
                    "Value:=", "1.93mm"
                ],
                [
                    "NAME:Bs2",
                    "Value:=", "8mm"
                ],
                [
                    "NAME:Rs",
                    "Value:=", "5mm"
                ],
                [
                    "NAME:SegAngle",
                    "Value:=", "0deg"
                ]
            ]
        ]
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.CreateUserDefinedPart(
    [
        "NAME:UserDefinedPrimitiveParameters",
        "DllName:=", "RMxprt/LapCoil.dll",
        "Version:=", "16.0",
        "NoOfParameters:=", 22,
        "Library:=", "syslib",
        [
            "NAME:ParamVector",
            [
                "NAME:Pair",
                "Name:=", "DiaGap",
                "Value:=", "161.90000000000001mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "DiaYoke",
                "Value:=", "269.24000000000001mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Length",
                "Value:=", "80mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Skew",
                "Value:=", "0deg"
            ],
            [
                "NAME:Pair",
                "Name:=", "Slots",
                "Value:=", "48"
            ],
            [
                "NAME:Pair",
                "Name:=", "SlotType",
                "Value:=", "0"
            ],
            [
                "NAME:Pair",
                "Name:=", "Hs0",
                "Value:=", "1.28mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Hs1",
                "Value:=", "1mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Hs2",
                "Value:=", "10mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Bs0",
                "Value:=", "2.5mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Bs1",
                "Value:=", "8mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Bs2",
                "Value:=", "5mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Rs",
                "Value:=", "0mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "FilletType",
                "Value:=", "0"
            ],
            [
                "NAME:Pair",
                "Name:=", "Layers",
                "Value:=", "2"
            ],
            [
                "NAME:Pair",
                "Name:=", "CoilPitch",
                "Value:=", "4"
            ],
            [
                "NAME:Pair",
                "Name:=", "EndExt",
                "Value:=", "5mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "SpanExt",
                "Value:=", "25mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "BendAngle",
                "Value:=", "0deg"
            ],
            [
                "NAME:Pair",
                "Name:=", "SegAngle",
                "Value:=", "10deg"
            ],
            [
                "NAME:Pair",
                "Name:=", "LenRegion",
                "Value:=", "200mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "InfoCoil",
                "Value:=", "0"
            ]
        ]
    ],
    [
        "NAME:Attributes",
        "Name:=", "LapCoil1",
        "Flags:=", "",
        "Color:=", "(143 175 143)",
        "Transparency:=", 0,
        "PartCoordinateSystem:=", "Global",
        "UDMId:=", "",
        "MaterialValue:=", "\"vacuum\"",
        "SurfaceMaterialValue:=", "\"\"",
        "SolveInside:=", True,
        "IsMaterialEditable:=", True
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DCmdTab",
            [
                "NAME:PropServers",
                "LapCoil1:CreateUserDefinedPart:1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Hs1",
                    "Value:=", "0.1mm"
                ],
                [
                    "NAME:Hs2",
                    "Value:=", "29.7mm"
                ],
                [
                    "NAME:Bs0",
                    "Value:=", "2.11mm"
                ],
                [
                    "NAME:Bs1",
                    "Value:=", "3.14mm"
                ],
                [
                    "NAME:Bs2",
                    "Value:=", "5.65mm"
                ],
                [
                    "NAME:Rs",
                    "Value:=", "2.5mm"
                ],
                [
                    "NAME:Layers",
                    "Value:=", "1"
                ],
                [
                    "NAME:CoilPitch",
                    "Value:=", "5"
                ],
                [
                    "NAME:SpanExt",
                    "Value:=", "18mm"
                ],
                [
                    "NAME:BendAngle",
                    "Value:=", "25deg"
                ],
                [
                    "NAME:InfoCoil",
                    "Value:=", "1"
                ]
            ]
        ]
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.Rotate(
    [
        "NAME:Selections",
        "Selections:=", "LapCoil1",
        "NewPartsModelFlag:=", "Model"
    ],
    [
        "NAME:RotateParameters",
        "RotateAxis:=", "Z",
        "RotateAngle:=", "7.5deg"
    ])
oEditor.DuplicateAroundAxis(
    [
        "NAME:Selections",
        "Selections:=", "LapCoil1",
        "NewPartsModelFlag:=", "Model"
    ],
    [
        "NAME:DuplicateAroundAxisParameters",
        "CreateNewObjects:=", True,
        "WhichAxis:=", "Z",
        "AngleStr:=", "15deg",
        "NumClones:=", "3"
    ],
    [
        "NAME:Options",
        "DuplicateAssignments:=", "false"
    ],
    [
        "CreateGroupsForNewObjects:=", False
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "LapCoil1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "PhaseA"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "LapCoil1_1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "PhaseB"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "LapCoil1_2"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "PhaseC"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "PhaseB"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Color",
                    "R:=", 0,
                    "G:=", 0,
                    "B:=", 255
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "PhaseC"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Color",
                    "R:=", 255,
                    "G:=", 0,
                    "B:=", 0
                ]
            ]
        ]
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.DuplicateAroundAxis(
    [
        "NAME:Selections",
        "Selections:=", "PhaseA,PhaseB,PhaseC",
        "NewPartsModelFlag:=", "Model"
    ],
    [
        "NAME:DuplicateAroundAxisParameters",
        "CreateNewObjects:=", True,
        "WhichAxis:=", "Z",
        "AngleStr:=", "45deg",
        "NumClones:=", "8"
    ],
    [
        "NAME:Options",
        "DuplicateAssignments:=", "false"
    ],
    [
        "CreateGroupsForNewObjects:=", False
    ])
oEditor.CreateUserDefinedPart(
    [
        "NAME:UserDefinedPrimitiveParameters",
        "DllName:=", "RMxprt/IPMCore.dll",
        "Version:=", "15.0",
        "NoOfParameters:=", 16,
        "Library:=", "syslib",
        [
            "NAME:ParamVector",
            [
                "NAME:Pair",
                "Name:=", "DiaGap",
                "Value:=", "160.40000000000001mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "DiaYoke",
                "Value:=", "110.64mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Length",
                "Value:=", "0mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Poles",
                "Value:=", "8"
            ],
            [
                "NAME:Pair",
                "Name:=", "PoleType",
                "Value:=", "3"
            ],
            [
                "NAME:Pair",
                "Name:=", "D1",
                "Value:=", "157.44mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "O1",
                "Value:=", "0mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "O2",
                "Value:=", "7.2800000000000002mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "B1",
                "Value:=", "4.7000000000000002mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "Rib",
                "Value:=", "14mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "HRib",
                "Value:=", "3mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "DminMag",
                "Value:=", "4.5mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "ThickMag",
                "Value:=", "6.4800000000000004mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "WidthMag",
                "Value:=", "32mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "LenRegion",
                "Value:=", "200mm"
            ],
            [
                "NAME:Pair",
                "Name:=", "InfoCore",
                "Value:=", "0"
            ]
        ]
    ],
    [
        "NAME:Attributes",
        "Name:=", "IPMCore1",
        "Flags:=", "",
        "Color:=", "(143 175 143)",
        "Transparency:=", 0,
        "PartCoordinateSystem:=", "Global",
        "UDMId:=", "",
        "MaterialValue:=", "\"vacuum\"",
        "SurfaceMaterialValue:=", "\"\"",
        "SolveInside:=", True,
        "IsMaterialEditable:=", True
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DCmdTab",
            [
                "NAME:PropServers",
                "PhaseA:CreateUserDefinedPart:1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Length",
                    "Value:=", "0mm"
                ]
            ]
        ]
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.Copy(
    [
        "NAME:Selections",
        "Selections:=", "IPMCore1"
    ])
oEditor.Paste()
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "IPMCore1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "rotor"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DCmdTab",
            [
                "NAME:PropServers",
                "IPMCore2:CreateUserDefinedPart:1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:InfoCore",
                    "Value:=", "1"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "IPMCore2"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "Magnets"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "Magnets"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Color",
                    "R:=", 255,
                    "G:=", 128,
                    "B:=", 0
                ]
            ]
        ]
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.CreatePolyline(
    [
        "NAME:PolylineParameters",
        "IsPolylineCovered:=", True,
        "IsPolylineClosed:=", False,
        [
            "NAME:PolylinePoints",
            [
                "NAME:PLPoint",
                "X:=", "0mm",
                "Y:=", "0mm",
                "Z:=", "0mm"
            ],
            [
                "NAME:PLPoint",
                "X:=", "200mm",
                "Y:=", "0mm",
                "Z:=", "0mm"
            ]
        ],
        [
            "NAME:PolylineSegments",
            [
                "NAME:PLSegment",
                "SegmentType:=", "Line",
                "StartIndex:=", 0,
                "NoOfPoints:=", 2
            ]
        ],
        [
            "NAME:PolylineXSection",
            "XSectionType:=", "None",
            "XSectionOrient:=", "Auto",
            "XSectionWidth:=", "0mm",
            "XSectionTopWidth:=", "0mm",
            "XSectionHeight:=", "0mm",
            "XSectionNumSegments:=", "0",
            "XSectionBendType:=", "Corner"
        ]
    ],
    [
        "NAME:Attributes",
        "Name:=", "Polyline1",
        "Flags:=", "",
        "Color:=", "(143 175 143)",
        "Transparency:=", 0,
        "PartCoordinateSystem:=", "Global",
        "UDMId:=", "",
        "MaterialValue:=", "\"vacuum\"",
        "SurfaceMaterialValue:=", "\"\"",
        "SolveInside:=", True,
        "IsMaterialEditable:=", True
    ])
oEditor.SweepAroundAxis(
    [
        "NAME:Selections",
        "Selections:=", "Polyline1",
        "NewPartsModelFlag:=", "Model"
    ],
    [
        "NAME:AxisSweepParameters",
        "DraftAngle:=", "0deg",
        "DraftType:=", "Round",
        "CheckFaceFaceIntersection:=", False,
        "SweepAxis:=", "Z",
        "SweepAngle:=", "45deg",
        "NumOfSegments:=", "5"
    ])
oEditor.Split(
    [
        "NAME:Selections",
        "Selections:=",
        "SlotCore1,PhaseA,PhaseB,PhaseC,PhaseA_1,PhaseA_2,PhaseA_3,PhaseA_4,PhaseA_5,PhaseA_6,PhaseA_7,PhaseB_1,PhaseB_2,PhaseB_3,PhaseB_4,PhaseB_5,PhaseB_6,PhaseB_7,PhaseC_1,PhaseC_2,PhaseC_3,PhaseC_4,PhaseC_5,PhaseC_6,PhaseC_7,rotor,Magnets",
        "NewPartsModelFlag:=", "Model"
    ],
    [
        "NAME:SplitToParameters",
        "SplitPlane:=", "ZX",
        "WhichSide:=", "PositiveOnly",
        "SplitCrossingObjectsOnly:=", False,
        "DeleteInvalidObjects:=", True
    ])
oEditor.Rotate(
    [
        "NAME:Selections",
        "Selections:=",
        "SlotCore1,PhaseA,PhaseB,PhaseC,PhaseA_1,PhaseA_2,PhaseA_3,PhaseA_7,PhaseB_1,PhaseB_2,PhaseB_3,PhaseB_7,PhaseC_1,PhaseC_2,PhaseC_3,PhaseC_7,rotor,Magnets",
        "NewPartsModelFlag:=", "Model"
    ],
    [
        "NAME:RotateParameters",
        "RotateAxis:=", "Z",
        "RotateAngle:=", "-45deg"
    ])
oEditor.Split(
    [
        "NAME:Selections",
        "Selections:=",
        "SlotCore1,PhaseA,PhaseB,PhaseC,PhaseA_1,PhaseA_2,PhaseA_3,PhaseA_7,PhaseB_1,PhaseB_2,PhaseB_3,PhaseB_7,PhaseC_1,PhaseC_2,PhaseC_3,PhaseC_7,rotor,Magnets",
        "NewPartsModelFlag:=", "Model"
    ],
    [
        "NAME:SplitToParameters",
        "SplitPlane:=", "ZX",
        "WhichSide:=", "NegativeOnly",
        "SplitCrossingObjectsOnly:=", False,
        "DeleteInvalidObjects:=", True
    ])
oEditor.Rotate(
    [
        "NAME:Selections",
        "Selections:=", "SlotCore1,PhaseA,PhaseB,PhaseC,PhaseA_7,PhaseB_7,PhaseC_7,rotor,Magnets",
        "NewPartsModelFlag:=", "Model"
    ],
    [
        "NAME:RotateParameters",
        "RotateAxis:=", "Z",
        "RotateAngle:=", "45deg"
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "PhaseA"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "PhaseA1"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "PhaseA_7"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "PhaseA_2"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "PhaseA1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "PhaseA_1"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "PhaseB"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "PhaseB_1"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "PhaseB_7"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "PhaseB_2"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "PhaseC"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "PhaseC_1"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "PhaseC_7"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "PhaseC_2"
                ]
            ]
        ]
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.SeparateBody(
    [
        "NAME:Selections",
        "Selections:=", "Magnets",
        "NewPartsModelFlag:=", "Model"
    ],
    [
        "CreateGroupsForNewObjects:=", False
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "Magnets"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "PM1"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "Magnets_Separate1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "PM2"
                ]
            ]
        ]
    ])
oDefinitionManager = oProject.GetDefinitionManager()
oDefinitionManager.AddMaterial(
    [
        "NAME:N36Z_20",
        "CoordinateSystemType:=", "Cartesian",
        "BulkOrSurfaceType:=", 1,
        [
            "NAME:PhysicsTypes",
            "set:=", ["Electromagnetic"]
        ],
        "permeability:=", "1.03",
        [
            "NAME:magnetic_coercivity",
            "property_type:=", "VectorProperty",
            "Magnitude:=", "-920000A_per_meter",
            "DirComp1:=", "1",
            "DirComp2:=", "0",
            "DirComp3:=", "0"
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "PM1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Material",
                    "Value:=", "\"N36Z_20\""
                ]
            ]
        ]
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.AssignMaterial(
    [
        "NAME:Selections",
        "Selections:=", "PM2"
    ],
    [
        "NAME:Attributes",
        "MaterialValue:=", "\"N36Z_20\"",
        "SolveInside:=", True,
        "IsMaterialEditable:=", True
    ])
oDefinitionManager.AddMaterial(
    [
        "NAME:M19_29G",
        "CoordinateSystemType:=", "Cartesian",
        "BulkOrSurfaceType:=", 1,
        [
            "NAME:PhysicsTypes",
            "set:=", ["Electromagnetic"]
        ],
        [
            "NAME:AttachedData",
            [
                "NAME:CoreLossMultiCurveData",
                "property_data:=", "coreloss_multi_curve_data",
                "coreloss_unit:=", "w_per_kg",
                [
                    "NAME:AllCurves",
                    [
                        "NAME:OneCurve",
                        "Frequency:=", "50Hz",
                        [
                            "NAME:Coordinates",
                            [
                                "NAME:Coordinate",
                                "X:=", 0,
                                "Y:=", 0
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.1,
                                "Y:=", 0.03
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.2,
                                "Y:=", 0.07
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.3,
                                "Y:=", 0.13
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.4,
                                "Y:=", 0.22
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.5,
                                "Y:=", 0.31
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.6,
                                "Y:=", 0.43
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.7,
                                "Y:=", 0.54
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.8,
                                "Y:=", 0.68
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.9,
                                "Y:=", 0.83
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1,
                                "Y:=", 1.01
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.1,
                                "Y:=", 1.2
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.2,
                                "Y:=", 1.42
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.3,
                                "Y:=", 1.7
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.4,
                                "Y:=", 2.12
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.5,
                                "Y:=", 2.47
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.6,
                                "Y:=", 2.8
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.7,
                                "Y:=", 3.05
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.8,
                                "Y:=", 3.25
                            ]
                        ]
                    ],
                    [
                        "NAME:OneCurve",
                        "Frequency:=", "100Hz",
                        [
                            "NAME:Coordinates",
                            [
                                "NAME:Coordinate",
                                "X:=", 0,
                                "Y:=", 0
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.1,
                                "Y:=", 0.04
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.2,
                                "Y:=", 0.16
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.3,
                                "Y:=", 0.34
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.4,
                                "Y:=", 0.55
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.5,
                                "Y:=", 0.8
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.6,
                                "Y:=", 1.08
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.7,
                                "Y:=", 1.38
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.8,
                                "Y:=", 1.73
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.9,
                                "Y:=", 2.1
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1,
                                "Y:=", 2.51
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.1,
                                "Y:=", 2.98
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.2,
                                "Y:=", 3.51
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.3,
                                "Y:=", 4.15
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.4,
                                "Y:=", 4.97
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.5,
                                "Y:=", 5.92
                            ]
                        ]
                    ],
                    [
                        "NAME:OneCurve",
                        "Frequency:=", "200Hz",
                        [
                            "NAME:Coordinates",
                            [
                                "NAME:Coordinate",
                                "X:=", 0,
                                "Y:=", 0
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.1,
                                "Y:=", 0.09
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.2,
                                "Y:=", 0.37
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.3,
                                "Y:=", 0.79
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.4,
                                "Y:=", 1.31
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.5,
                                "Y:=", 1.91
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.6,
                                "Y:=", 2.61
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.7,
                                "Y:=", 3.39
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.8,
                                "Y:=", 4.26
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.9,
                                "Y:=", 5.23
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1,
                                "Y:=", 6.3
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.1,
                                "Y:=", 7.51
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.2,
                                "Y:=", 8.88
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.3,
                                "Y:=", 10.5
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.4,
                                "Y:=", 12.5
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.5,
                                "Y:=", 14.9
                            ]
                        ]
                    ],
                    [
                        "NAME:OneCurve",
                        "Frequency:=", "400Hz",
                        [
                            "NAME:Coordinates",
                            [
                                "NAME:Coordinate",
                                "X:=", 0,
                                "Y:=", 0
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.1,
                                "Y:=", 0.21
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.2,
                                "Y:=", 0.92
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.3,
                                "Y:=", 1.99
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.4,
                                "Y:=", 3.33
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.5,
                                "Y:=", 4.94
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.6,
                                "Y:=", 6.84
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.7,
                                "Y:=", 9
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.8,
                                "Y:=", 11.4
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.9,
                                "Y:=", 14.2
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1,
                                "Y:=", 17.3
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.1,
                                "Y:=", 20.9
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.2,
                                "Y:=", 24.9
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.3,
                                "Y:=", 29.5
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.4,
                                "Y:=", 35.4
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.5,
                                "Y:=", 41.8
                            ]
                        ]
                    ],
                    [
                        "NAME:OneCurve",
                        "Frequency:=", "1000Hz",
                        [
                            "NAME:Coordinates",
                            [
                                "NAME:Coordinate",
                                "X:=", 0,
                                "Y:=", 0
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.1,
                                "Y:=", 0.99
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.2,
                                "Y:=", 3.67
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.3,
                                "Y:=", 7.63
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.4,
                                "Y:=", 12.7
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.5,
                                "Y:=", 18.9
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.6,
                                "Y:=", 26.4
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.7,
                                "Y:=", 35.4
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.8,
                                "Y:=", 46
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 0.9,
                                "Y:=", 58.4
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1,
                                "Y:=", 73
                            ],
                            [
                                "NAME:Coordinate",
                                "X:=", 1.1,
                                "Y:=", 90.1
                            ]
                        ]
                    ]
                ]
            ]
        ],
        [
            "NAME:permeability",
            "property_type:=", "nonlinear",
            "BType:=", "normal",
            "HUnit:=", "A_per_meter",
            "BUnit:=", "tesla",
            [
                "NAME:BHCoordinates",
                [
                    "NAME:Coordinate",
                    "X:=", 0,
                    "Y:=", 0
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 22.28,
                    "Y:=", 0.05
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 25.46,
                    "Y:=", 0.1
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 31.83,
                    "Y:=", 0.15
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 47.74,
                    "Y:=", 0.36
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 63.66,
                    "Y:=", 0.54
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 79.57,
                    "Y:=", 0.65
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 159.15,
                    "Y:=", 0.99
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 318.3,
                    "Y:=", 1.2
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 477.46,
                    "Y:=", 1.28
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 636.61,
                    "Y:=", 1.33
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 795.77,
                    "Y:=", 1.36
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 1591.5,
                    "Y:=", 1.44
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 3183,
                    "Y:=", 1.52
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 4774.6,
                    "Y:=", 1.58
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 6366.1,
                    "Y:=", 1.63
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 7957.7,
                    "Y:=", 1.67
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 15915,
                    "Y:=", 1.8
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 31830,
                    "Y:=", 1.9
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 111407,
                    "Y:=", 2
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 190984,
                    "Y:=", 2.1
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 350138,
                    "Y:=", 2.3
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 509252,
                    "Y:=", 2.5
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 560177.2,
                    "Y:=", 2.563994494
                ],
                [
                    "NAME:Coordinate",
                    "X:=", 1527756,
                    "Y:=", 3.779889874
                ]
            ]
        ],
        [
            "NAME:magnetic_coercivity",
            "property_type:=", "VectorProperty",
            "Magnitude:=", "0A_per_meter",
            "DirComp1:=", "1",
            "DirComp2:=", "0",
            "DirComp3:=", "0"
        ],
        [
            "NAME:core_loss_type",
            "property_type:=", "ChoiceProperty",
            "Choice:=", "Electrical Steel"
        ],
        "core_loss_kh:=", "184.233670546732",
        "core_loss_kc:=", "0.386260592696451",
        "core_loss_ke:=", "0.270231418332487",
        "core_loss_kdc:=", "0",
        "mass_density:=", "7872"
    ])
oEditor.AssignMaterial(
    [
        "NAME:Selections",
        "Selections:=", "rotor"
    ],
    [
        "NAME:Attributes",
        "MaterialValue:=", "\"M19_29G\"",
        "SolveInside:=", True,
        "IsMaterialEditable:=", True
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DCmdTab",
            [
                "NAME:PropServers",
                "SlotCore1:CreateUserDefinedPart:1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Length",
                    "Value:=", "0mm"
                ]
            ]
        ]
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.AssignMaterial(
    [
        "NAME:Selections",
        "Selections:=", "SlotCore1"
    ],
    [
        "NAME:Attributes",
        "MaterialValue:=", "\"M19_29G\"",
        "SolveInside:=", True,
        "IsMaterialEditable:=", True
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "PhaseB_1",
                "PhaseC_1",
                "PhaseA_1",
                "PhaseA_2",
                "PhaseB_2",
                "PhaseC_2"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Material",
                    "Value:=", "\"copper\""
                ]
            ]
        ]
    ])
oModule = oDesign.GetModule("BoundarySetup")
oModule.AssignCurrentGroup(["PhaseA1_1", "PhaseA1_2", "PhaseA1_3", "PhaseA1_4", "PhaseA1_5", "PhaseA1_6"],
                           [
                               "NAME:PhaseA1_1",
                               "Objects:=", ["PhaseA_1", "PhaseA_2", "PhaseB_1", "PhaseB_2", "PhaseC_1", "PhaseC_2"],
                               "Current:=", "1500A",
                               "IsSolid:=", True,
                               "IsPositive:=", True
                           ])
oModule.EditCurrent("PhaseA1_2",
                    [
                        "NAME:PhaseA2",
                        "Current:=", "1500A",
                        "IsSolid:=", True,
                        "IsPositive:=", True
                    ])
oModule.EditCurrent("PhaseA1_1",
                    [
                        "NAME:PhaseA1",
                        "Current:=", "1500A",
                        "IsSolid:=", True,
                        "IsPositive:=", True
                    ])
oModule.EditCurrent("PhaseA1_5",
                    [
                        "NAME:PhaseC1",
                        "Current:=", "-750A",
                        "IsSolid:=", True,
                        "IsPositive:=", False
                    ])
oModule.AssignCoilGroup(["PhaseA_1", "PhaseA_2", "PhaseB_1", "PhaseB_2", "PhaseC_1", "PhaseC_2"],
                        [
                            "NAME:PhaseA_1",
                            "Objects:=", ["PhaseA_1", "PhaseA_2", "PhaseB_1", "PhaseB_2", "PhaseC_1", "PhaseC_2"],
                            "Conductor number:=", "9",
                            "PolarityType:=", "Positive"
                        ])
oModule.DeleteBoundaries(["PhaseC1"])
oModule.DeleteBoundaries(["PhaseA2"])
oModule.DeleteBoundaries(["PhaseA1_6"])
oModule.DeleteBoundaries(["PhaseA1_4"])
oModule.DeleteBoundaries(["PhaseA1_3"])
oModule.DeleteBoundaries(["PhaseA1"])
oModule.EditCoil("PhaseC_2",
                 [
                     "NAME:PhaseC_2",
                     "Conductor number:=", "9",
                     "PolarityType:=", "Negative"
                 ])
oModule.EditCoil("PhaseC_1",
                 [
                     "NAME:PhaseC_1",
                     "Conductor number:=", "9",
                     "PolarityType:=", "Negative"
                 ])
oDesign.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:LocalVariableTab",
            [
                "NAME:PropServers",
                "LocalVariables"
            ],
            [
                "NAME:NewProps",
                [
                    "NAME:Poles",
                    "PropType:=", "VariableProp",
                    "UserDef:=", True,
                    "Value:=", "8"
                ],
                [
                    "NAME:PolePairs",
                    "PropType:=", "VariableProp",
                    "UserDef:=", True,
                    "Value:=", "Poles/2"
                ],
                [
                    "NAME:Speed_rpm",
                    "PropType:=", "VariableProp",
                    "UserDef:=", True,
                    "Value:=", "3000"
                ],
                [
                    "NAME:Omega",
                    "PropType:=", "VariableProp",
                    "UserDef:=", True,
                    "Value:=", "360*Speed_rpm*PolePairs/60"
                ],
                [
                    "NAME:Omega_rad",
                    "PropType:=", "VariableProp",
                    "UserDef:=", True,
                    "Value:=", "Omega * pi / 180"
                ],
                [
                    "NAME:Thet_deg",
                    "PropType:=", "VariableProp",
                    "UserDef:=", True,
                    "Value:=", "20"
                ],
                [
                    "NAME:Thet",
                    "PropType:=", "VariableProp",
                    "UserDef:=", True,
                    "Value:=", "Thet_deg * pi /180"
                ],
                [
                    "NAME:Imax",
                    "PropType:=", "VariableProp",
                    "UserDef:=", True,
                    "Value:=", "250A"
                ]
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:PolePairs",
                    "Value:=", "Poles/2"
                ],
                [
                    "NAME:Imax",
                    "Value:=", "250A"
                ]
            ]
        ]
    ])
oModule.AssignWindingGroup(
    [
        "NAME:PhaseA",
        "Type:=", "Current",
        "IsSolid:=", True,
        "Current:=", "Imax*sin(Omega_rad*Time+Thet)",
        "Resistance:=", "0ohm",
        "Inductance:=", "0nH",
        "Voltage:=", "0mV",
        "ParallelBranchesNum:=", "1"
    ])
oModule.EditWindingGroup("PhaseA",
                         [
                             "NAME:PhaseA",
                             "Type:=", "Current",
                             "IsSolid:=", True,
                             "Current:=", "Imax*sin(Omega_rad*Time+Thet)",
                             "Resistance:=", "0ohm",
                             "Inductance:=", "0nH",
                             "Voltage:=", "0mV",
                             "ParallelBranchesNum:=", "1"
                         ])
oModule.EditCoil("PhaseA_1",
                 [
                     "NAME:PhaseA_1",
                     "Conductor number:=", "9",
                     "PolarityType:=", "Positive"
                 ])
oModule.EditWindingGroup("PhaseA",
                         [
                             "NAME:PhaseA",
                             "Type:=", "Current",
                             "IsSolid:=", False,
                             "Current:=", "Imax*sin(Omega_rad*Time+Thet)",
                             "Resistance:=", "0ohm",
                             "Inductance:=", "0nH",
                             "Voltage:=", "0mV",
                             "ParallelBranchesNum:=", "1"
                         ])
oModule.AddCoilstoWinding(
    [
        "NAME:AddTerminalsToWinding",
        [
            "NAME:BoundaryList",
            [
                "NAME:PhaseA_1",
                "Objects:=", ["PhaseA_1"],
                "ParentBndID:=", "PhaseA",
                "Conductor number:=", "9",
                "Winding:=", "PhaseA",
                "PolarityType:=", "Positive"
            ],
            [
                "NAME:PhaseA_2",
                "Objects:=", ["PhaseA_2"],
                "ParentBndID:=", "PhaseA",
                "Conductor number:=", "9",
                "Winding:=", "PhaseA",
                "PolarityType:=", "Positive"
            ]
        ]
    ])
oModule.AssignWindingGroup(
    [
        "NAME:PhaseB",
        "Type:=", "Current",
        "IsSolid:=", False,
        "Current:=", "Imax*sin(Omega_rad*Time-2*pi/3+Thet)",
        "Resistance:=", "0ohm",
        "Inductance:=", "0nH",
        "Voltage:=", "0mV",
        "ParallelBranchesNum:=", "1"
    ])
oModule.AssignWindingGroup(
    [
        "NAME:PhaseC",
        "Type:=", "Current",
        "IsSolid:=", False,
        "Current:=", "Imax*sin(Omega_rad*Time+2*pi/3+Thet)",
        "Resistance:=", "0ohm",
        "Inductance:=", "0nH",
        "Voltage:=", "0mV",
        "ParallelBranchesNum:=", "1"
    ])
oModule.AddCoilstoWinding(
    [
        "NAME:AddTerminalsToWinding",
        [
            "NAME:BoundaryList",
            [
                "NAME:PhaseB_1",
                "Objects:=", ["PhaseB_1"],
                "ParentBndID:=", "PhaseB",
                "Conductor number:=", "9",
                "Winding:=", "PhaseB",
                "PolarityType:=", "Positive"
            ],
            [
                "NAME:PhaseB_2",
                "Objects:=", ["PhaseB_2"],
                "ParentBndID:=", "PhaseB",
                "Conductor number:=", "9",
                "Winding:=", "PhaseB",
                "PolarityType:=", "Positive"
            ]
        ]
    ])
oModule.AddCoilstoWinding(
    [
        "NAME:AddTerminalsToWinding",
        [
            "NAME:BoundaryList",
            [
                "NAME:PhaseC_1",
                "Objects:=", ["PhaseC_1"],
                "ParentBndID:=", "PhaseC",
                "Conductor number:=", "9",
                "Winding:=", "PhaseC",
                "PolarityType:=", "Negative"
            ],
            [
                "NAME:PhaseC_2",
                "Objects:=", ["PhaseC_2"],
                "ParentBndID:=", "PhaseC",
                "Conductor number:=", "9",
                "Winding:=", "PhaseC",
                "PolarityType:=", "Negative"
            ]
        ]
    ])
oModule.SetCoreLoss(["SlotCore1", "rotor"], False)
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.Copy(
    [
        "NAME:Selections",
        "Selections:=", "Polyline1"
    ])
oEditor.Paste()
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DAttributeTab",
            [
                "NAME:PropServers",
                "Polyline1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Name",
                    "Value:=", "band"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DPolylineTab",
            [
                "NAME:PropServers",
                "band:CreatePolyline:2:Segment0"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Point2",
                    "X:=", "80.575mm",
                    "Y:=", "0mm",
                    "Z:=", "0mm"
                ]
            ]
        ]
    ])
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DCmdTab",
            [
                "NAME:PropServers",
                "band:SweepAroundAxis:1"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Number of Segments",
                    "Value:=", "0"
                ]
            ]
        ]
    ])
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.Copy(
    [
        "NAME:Selections",
        "Selections:=", "band"
    ])
oEditor.Paste()
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.ChangeProperty(
    [
        "NAME:AllTabs",
        [
            "NAME:Geometry3DPolylineTab",
            [
                "NAME:PropServers",
                "band1:CreatePolyline:2:Segment0"
            ],
            [
                "NAME:ChangedProps",
                [
                    "NAME:Point2",
                    "X:=", "80.4mm",
                    "Y:=", "0mm",
                    "Z:=", "0mm"
                ]
            ]
        ]
    ])
oModule.AssignMaster(
    [
        "NAME:Master1",
        "Edges:=", [41634],
        "ReverseV:=", False
    ])
oModule.AssignSlave(
    [
        "NAME:Slave1",
        "Edges:=", [41645],
        "ReverseU:=", True,
        "Master:=", "Master1",
        "SameAsMaster:=", False
    ])
oModule.AssignVectorPotential(
    [
        "NAME:zeroflux",
        "Edges:=", [41644, 41643, 41642, 41641, 41640, 41639],
        "Value:=", "0",
        "CoordinateSystem:=", ""
    ])
oModule = oDesign.GetModule("ModelSetup")
oModule.AssignBand(
    [
        "NAME:Data",
        "Move Type:=", "Rotate",
        "Coordinate System:=", "Global",
        "Axis:=", "Z",
        "Is Positive:=", True,
        "InitPos:=", "30deg",
        "HasRotateLimit:=", False,
        "NonCylindrical:=", False,
        "Consider Mechanical Transient:=", False,
        "Angular Velocity:=", "3000rpm",
        "Objects:=", ["band"]
    ])
oDesign.SetDesignSettings(
    [
        "NAME:Design Settings Data",
        "Perform Minimal validation:=", False,
        "EnabledObjects:=", [],
        "PreserveTranSolnAfterDatasetEdit:=", False,
        "ComputeTransientInductance:=", False,
        "ComputeIncrementalMatrix:=", False,
        "PerfectConductorThreshold:=", 1E+030,
        "InsulatorThreshold:=", 1,
        "ModelDepth:=", "83.82mm",
        "UseSkewModel:=", False,
        "EnableTranTranLinkWithSimplorer:=", False,
        "BackgroundMaterialName:=", "vacuum",
        "Multiplier:=", "8"
    ],
    [
        "NAME:Model Validation Settings",
        "EntityCheckLevel:=", "Strict",
        "IgnoreUnclassifiedObjects:=", False,
        "SkipIntersectionChecks:=", False
    ])
oModule = oDesign.GetModule("AnalysisSetup")
oModule.InsertSetup("Transient",
                    [
                        "NAME:Setup1",
                        "Enabled:=", True,
                        "NonlinearSolverResidual:=", "0.0001",
                        "TimeIntegrationMethod:=", 0,
                        "StopTime:=", "15ms",
                        "TimeStep:=", "250us",
                        "OutputError:=", False,
                        "UseControlProgram:=", False,
                        "ControlProgramName:=", " ",
                        "ControlProgramArg:=", " ",
                        "CallCtrlProgAfterLastStep:=", False,
                        "FastReachSteadyState:=", False,
                        "IsGeneralTransient:=", True,
                        "HasSweepSetup:=", False,
                        "UseAdaptiveTimeStep:=", False,
                        "InitialTimeStep:=", "0.002s",
                        "MinTimeStep:=", "0.001s",
                        "MaxTimeStep:=", "0.003s",
                        "TimeStepErrTolerance:=", 0.0001
                    ])
oModule = oDesign.GetModule("ReportSetup")
oModule.CreateReport("Torque Plot 1", "Transient", "Rectangular Plot", "Setup1 : Transient",
                     [
                         "Domain:=", "Sweep"
                     ],
                     [
                         "Time:=", ["All"],
                         "Poles:=", ["Nominal"],
                         "Speed_rpm:=", ["Nominal"],
                         "Thet_deg:=", ["Nominal"],
                         "Imax:=", ["Nominal"]
                     ],
                     [
                         "X Component:=", "Time",
                         "Y Component:=", ["Moving1.Torque"]
                     ], [])
oModule = oDesign.GetModule("OutputVariable")
oModule.CreateOutputVariable("t1", "Moving1.Torque", "Setup1 : Transient", "Transient", [])
oModule = oDesign.GetModule("ReportSetup")
oModule.CreateReport("Output Variables Plot 1", "Transient", "Rectangular Plot", "Setup1 : Transient",
                     [
                         "Domain:=", "Sweep"
                     ],
                     [
                         "Time:=", ["All"],
                         "Poles:=", ["Nominal"],
                         "Speed_rpm:=", ["Nominal"],
                         "Thet_deg:=", ["Nominal"],
                         "Imax:=", ["Nominal"]
                     ],
                     [
                         "X Component:=", "Time",
                         "Y Component:=", ["t1"]
                     ], [])
oProject.Save()
