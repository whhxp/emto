from matplotlib import pyplot as plt
import random
import math
from point_class import Point

points = []
A = Point(6, 10)
B = Point(10, 10)
C = Point(2, 0)

for i in range(5000):

    ab = B - A
    ac = C - A

    x = random.uniform(0, 1)
    y = random.uniform(0, 1)

    if x + y > 1:
        xx = 1 - x
        yy = 1 - y
    else:
        xx = x
        yy = y

    p1 = A + xx * ab + yy * ac
    # print(p1.x,p1.y)
    points.append(p1)

x1 = [A.x, B.x, C.x, A.x]
y1 = [A.y, B.y, C.y, A.y]
X = []
Y = []
for p in points:
    X.append(p.x)
    Y.append(p.y)

plt.axis('equal')
plt.plot(x1, y1)
plt.scatter(X, Y)

plt.show()
